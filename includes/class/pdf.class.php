<?php
include('../../includes/fpdf/fpdf.php');
include '../../conf/_connect.php';

class Courrier_parent extends FPDF
{
    var $widths;
    var $aligns;

    /*
     * justification et mise en forme du texte
     */
    var $wLine; // Largeur maximale de la ligne
    var $hLine; // Hauteur de la ligne
    var $Text; // Texte &agrave; afficher
    var $border;
    var $align; // Justification du texte
    var $fill;
    var $Padding;
    var $lPadding;
    var $tPadding;
    var $bPadding;
    var $rPadding;
    var $TagStyle; // Style associé &agrave; chaque balise
    var $Indent;
    var $Space; // Espace minimum entre les mots
    var $PileStyle;
    var $Line2Print; // Ligne &agrave; afficher
    var $NextLineBegin; // Tampon entre lignes
    var $TagName; // Tableau
    var $Delta; // Largeur maximale moins largeur
    var $StringLength;
    var $LineLength;
    var $wTextLine; //Largeur moins les "padding"
    var $nbSpace; // Nombre d'espaces dans la ligne
    var $Xini; // Position initiale
    var $href; // URL courante
    var $TagHref; // URL associée &agrave; une cellule

    // Fonctions Publiques

    function WriteTag($w, $h, $txt, $border=0, $align="J", $fill=false, $padding=0)
    {
        $this->wLine=$w;
        $this->hLine=$h;
        $this->Text=trim($txt);
        $this->Text=preg_replace("/\n|\r|\t/","",$this->Text);
        $this->border=$border;
        $this->align=$align;
        $this->fill=$fill;
        $this->Padding=$padding;

        $this->Xini=$this->GetX();
        $this->href="";
        $this->PileStyle=array();
        $this->TagHref=array();
        $this->LastLine=false;

        $this->SetSpace();
        $this->Padding();
        $this->LineLength();
        $this->BorderTop();

        while($this->Text!="")
        {
            $this->MakeLine();
            $this->PrintLine();
        }

        $this->BorderBottom();
    }


    function SetStyle($tag, $family, $style, $size, $color, $indent=-1)
    {
         $tag=trim($tag);
         $this->TagStyle[$tag]['family']=trim($family);
         $this->TagStyle[$tag]['style']=trim($style);
         $this->TagStyle[$tag]['size']=trim($size);
         $this->TagStyle[$tag]['color']=trim($color);
         $this->TagStyle[$tag]['indent']=$indent;
    }


    // Fonctions privées

    function SetSpace() // Espace minimal entre les mots
    {
        $tag=$this->Parser($this->Text);
        $this->FindStyle($tag[2],0);
        $this->DoStyle(0);
        $this->Space=$this->GetStringWidth(" ");
    }


    function Padding()
    {
        if(preg_match("/^.+,/",$this->Padding)) {
            $tab=explode(",",$this->Padding);
            $this->lPadding=$tab[0];
            $this->tPadding=$tab[1];
            if(isset($tab[2]))
                $this->bPadding=$tab[2];
            else
                $this->bPadding=$this->tPadding;
            if(isset($tab[3]))
                $this->rPadding=$tab[3];
            else
                $this->rPadding=$this->lPadding;
        }
        else
        {
            $this->lPadding=$this->Padding;
            $this->tPadding=$this->Padding;
            $this->bPadding=$this->Padding;
            $this->rPadding=$this->Padding;
        }
        if($this->tPadding<$this->LineWidth)
            $this->tPadding=$this->LineWidth;
    }


    function LineLength()
    {
        if($this->wLine==0)
            $this->wLine=$this->w - $this->Xini - $this->rMargin;

        $this->wTextLine = $this->wLine - $this->lPadding - $this->rPadding;
    }


    function BorderTop()
    {
        $border=0;
        if($this->border==1)
            $border="TLR";
        $this->Cell($this->wLine,$this->tPadding,"",$border,0,'C',$this->fill);
        $y=$this->GetY()+$this->tPadding;
        $this->SetXY($this->Xini,$y);
    }


    function BorderBottom()
    {
        $border=0;
        if($this->border==1)
            $border="BLR";
        $this->Cell($this->wLine,$this->bPadding,"",$border,0,'C',$this->fill);
    }


    function DoStyle($tag) // Applique un style
    {
        $tag=trim($tag);
        $this->SetFont($this->TagStyle[$tag]['family'],
            $this->TagStyle[$tag]['style'],
            $this->TagStyle[$tag]['size']);

        $tab=explode(",",$this->TagStyle[$tag]['color']);
        if(count($tab)==1)
            $this->SetTextColor($tab[0]);
        else
            $this->SetTextColor($tab[0],$tab[1],$tab[2]);
    }


    function FindStyle($tag, $ind) // Héritage des éléments parents
    {
        $tag=trim($tag);

        // Famille
        if($this->TagStyle[$tag]['family']!="")
            $family=$this->TagStyle[$tag]['family'];
        else
        {
            reset($this->PileStyle);
            while(list($k,$val)=each($this->PileStyle))
            {
                $val=trim($val);
                if($this->TagStyle[$val]['family']!="") {
                    $family=$this->TagStyle[$val]['family'];
                    break;
                }
            }
        }

        // Style
        $style="";
        $style1=strtoupper($this->TagStyle[$tag]['style']);
        if($style1!="N")
        {
            $bold=false;
            $italic=false;
            $underline=false;
            reset($this->PileStyle);
            while(list($k,$val)=each($this->PileStyle))
            {
                $val=trim($val);
                $style1=strtoupper($this->TagStyle[$val]['style']);
                if($style1=="N")
                    break;
                else
                {
                    if(strpos($style1,"B")!==false)
                        $bold=true;
                    if(strpos($style1,"I")!==false)
                        $italic=true;
                    if(strpos($style1,"U")!==false)
                        $underline=true;
                }
            }
            if($bold)
                $style.="B";
            if($italic)
                $style.="I";
            if($underline)
                $style.="U";
        }

        // Taille
        if($this->TagStyle[$tag]['size']!=0)
            $size=$this->TagStyle[$tag]['size'];
        else
        {
            reset($this->PileStyle);
            while(list($k,$val)=each($this->PileStyle))
            {
                $val=trim($val);
                if($this->TagStyle[$val]['size']!=0) {
                    $size=$this->TagStyle[$val]['size'];
                    break;
                }
            }
        }

        // Couleur
        if($this->TagStyle[$tag]['color']!="")
            $color=$this->TagStyle[$tag]['color'];
        else
        {
            reset($this->PileStyle);
            while(list($k,$val)=each($this->PileStyle))
            {
                $val=trim($val);
                if($this->TagStyle[$val]['color']!="") {
                    $color=$this->TagStyle[$val]['color'];
                    break;
                }
            }
        }

        // Résultat
        $this->TagStyle[$ind]['family']=$family;
        $this->TagStyle[$ind]['style']=$style;
        $this->TagStyle[$ind]['size']=$size;
        $this->TagStyle[$ind]['color']=$color;
        $this->TagStyle[$ind]['indent']=$this->TagStyle[$tag]['indent'];
    }


    function Parser($text)
    {
        $tab=array();
        // Balise fermante
        if(preg_match("|^(</([^>]+)>)|",$text,$regs)) {
            $tab[1]="c";
            $tab[2]=trim($regs[2]);
        }
        // Balise ouvrante
        else if(preg_match("|^(<([^>]+)>)|",$text,$regs)) {
            $regs[2]=preg_replace("/^a/","a ",$regs[2]); // Rustine : l'espace disparaît
            $tab[1]="o";
            $tab[2]=trim($regs[2]);

            // Présence d'attributs
            if(preg_match("/(.+) (.+)='(.+)'/",$regs[2])) {
                $tab1=preg_split("/ +/",$regs[2]);
                $tab[2]=trim($tab1[0]);
                while(list($i,$couple)=each($tab1))
                {
                    if($i>0) {
                        $tab2=explode("=",$couple);
                        $tab2[0]=trim($tab2[0]);
                        $tab2[1]=trim($tab2[1]);
                        $end=strlen($tab2[1])-2;
                        $tab[$tab2[0]]=substr($tab2[1],1,$end);
                    }
                }
            }
        }
         // Espace
         else if(preg_match("/^( )/",$text,$regs)) {
            $tab[1]="s";
            $tab[2]=' ';
        }
        // Texte
        else if(preg_match("/^([^< ]+)/",$text,$regs)) {
            $tab[1]="t";
            $tab[2]=trim($regs[1]);
        }
        // Elagage
        $begin=strlen($regs[1]);
         $end=strlen($text);
         $text=substr($text, $begin, $end);
        $tab[0]=$text;

        return $tab; // 0 : $text; 1 : type de balise (tag); 2 : élément
    }


    function MakeLine() // Fabrique une ligne
    {
        $this->Text.=" ";
        $this->LineLength=array();
        $this->TagHref=array();
        $Length=0;
        $this->nbSpace=0;

        $i=$this->BeginLine();
        $this->TagName=array();

        if($i==0) {
            $Length=$this->StringLength[0];
            $this->TagName[0]=1;
            $this->TagHref[0]=$this->href;
        }

        while($Length<$this->wTextLine)
        {
            $tab=$this->Parser($this->Text);
            $this->Text=$tab[0];
            if($this->Text=="") {
                $this->LastLine=true;
                break;
            }

            if($tab[1]=="o") {
                array_unshift($this->PileStyle,$tab[2]);
                $this->FindStyle($this->PileStyle[0],$i+1);

                $this->DoStyle($i+1);
                $this->TagName[$i+1]=1;
                if($this->TagStyle[$tab[2]]['indent']!=-1) {
                    $Length+=$this->TagStyle[$tab[2]]['indent'];
                    $this->Indent=$this->TagStyle[$tab[2]]['indent'];
                }
                if($tab[2]=="a")
                    $this->href=$tab['href'];
            }

            if($tab[1]=="c") {
                array_shift($this->PileStyle);
                if(isset($this->PileStyle[0]))
                {
                    $this->FindStyle($this->PileStyle[0],$i+1);
                    $this->DoStyle($i+1);
                }
                $this->TagName[$i+1]=1;
                if($this->TagStyle[$tab[2]]['indent']!=-1) {
                    $this->LastLine=true;
                    $this->Text=trim($this->Text);
                    break;
                }
                if($tab[2]=="a")
                    $this->href="";
            }

            if($tab[1]=="s") {
                $i++;
                $Length+=$this->Space;
                $this->Line2Print[$i]="";
                if($this->href!="")
                    $this->TagHref[$i]=$this->href;
            }

            if($tab[1]=="t") {
                $i++;
                $this->StringLength[$i]=$this->GetStringWidth($tab[2]);
                $Length+=$this->StringLength[$i];
                $this->LineLength[$i]=$Length;
                $this->Line2Print[$i]=$tab[2];
                if($this->href!="")
                    $this->TagHref[$i]=$this->href;
             }

        }

        trim($this->Text);
        if($Length>$this->wTextLine || $this->LastLine==true)
            $this->EndLine();
    }


    function BeginLine()
    {
        $this->Line2Print=array();
        $this->StringLength=array();

        if(isset($this->PileStyle[0]))
        {
            $this->FindStyle($this->PileStyle[0],0);
            $this->DoStyle(0);
        }

        if(count($this->NextLineBegin)>0) {
            $this->Line2Print[0]=$this->NextLineBegin['text'];
            $this->StringLength[0]=$this->NextLineBegin['length'];
            $this->NextLineBegin=array();
            $i=0;
        }
        else {
            preg_match("/^(( *(<([^>]+)>)* *)*)(.*)/",$this->Text,$regs);
            $regs[1]=str_replace(" ", "", $regs[1]);
            $this->Text=$regs[1].$regs[5];
            $i=-1;
        }

        return $i;
    }


    function EndLine()
    {
        if(end($this->Line2Print)!="" && $this->LastLine==false) {
            $this->NextLineBegin['text']=array_pop($this->Line2Print);
            $this->NextLineBegin['length']=end($this->StringLength);
            array_pop($this->LineLength);
        }

        while(end($this->Line2Print)==="")
            array_pop($this->Line2Print);

        $this->Delta=$this->wTextLine-end($this->LineLength);

        $this->nbSpace=0;
        for($i=0; $i<count($this->Line2Print); $i++) {
            if($this->Line2Print[$i]=="")
                $this->nbSpace++;
        }
    }


    function PrintLine()
    {
        $border=0;
        if($this->border==1)
            $border="LR";
        $this->Cell($this->wLine,$this->hLine,"",$border,0,'C',$this->fill);
        $y=$this->GetY();
        $this->SetXY($this->Xini+$this->lPadding,$y);

        if($this->Indent!=-1) {
            if($this->Indent!=0)
                $this->Cell($this->Indent,$this->hLine);
            $this->Indent=-1;
        }

        $space=$this->LineAlign();
        $this->DoStyle(0);
        for($i=0; $i<count($this->Line2Print); $i++)
        {
            if(isset($this->TagName[$i]))
                $this->DoStyle($i);
            if(isset($this->TagHref[$i]))
                $href=$this->TagHref[$i];
            else
                $href='';
            if($this->Line2Print[$i]=="")
                $this->Cell($space,$this->hLine,"         ",0,0,'C',false,$href);
            else
                $this->Cell($this->StringLength[$i],$this->hLine,$this->Line2Print[$i],0,0,'C',false,$href);
        }

        $this->LineBreak();
        if($this->LastLine && $this->Text!="")
            $this->EndParagraph();
        $this->LastLine=false;
    }


    function LineAlign()
    {
        $space=$this->Space;
        if($this->align=="J") {
            if($this->nbSpace!=0)
                $space=$this->Space + ($this->Delta/$this->nbSpace);
            if($this->LastLine)
                $space=$this->Space;
        }

        if($this->align=="R")
            $this->Cell($this->Delta,$this->hLine);

        if($this->align=="C")
            $this->Cell($this->Delta/2,$this->hLine);

        return $space;
    }


    function LineBreak() // Retour à la ligne
    {
        $x=$this->Xini;
        $y=$this->GetY()+$this->hLine;
        $this->SetXY($x,$y);
    }


    function EndParagraph() // Interligne entre paragraphes
    {
        $border=0;
        if($this->border==1)
            $border="LR";
        $this->Cell($this->wLine,$this->hLine/2,"",$border,0,'C',$this->fill);
        $x=$this->Xini;
        $y=$this->GetY()+$this->hLine/2;
        $this->SetXY($x,$y);
    }




    /*
     * Fin justification et mise en forme du texte
     */

    function SetWidths($w)
    {
        //Tableau des largeurs de colonnes
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Tableau des alignements de colonnes
        $this->aligns=$a;
    }




    /*function WriteHTML($html)	//,$position
    {
        //Parseur HTML
        //$html=strip_tags($html,'<b><u><i><a><img><p><br><strong><em><font><tr><blockquote>'); //supprime tous les tags sauf ceux reconnus
        $html=str_replace("\n",' ',$html);
        $a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
        foreach($a as $i=>$e)
        {
            if($i%2==0)
            {
                //Texte
                if($this->HREF)
                $this->PutLink($this->HREF,$e);
                elseif($this->ALIGN == 'center')
                $this->Cell(0,3,$e,0,1,'C');
                else
                //$this->WriteFlowingBlock($e);
                $this->Write(5,$e);
            }
            else
            {
                //Balise
                if($e{0}=='/')
                $this->CloseTag(strtoupper(substr($e,1)),$position);
                else
                {
                    //Extraction des attributs
                    $a2=explode(' ',$e);
                    $tag=strtoupper(array_shift($a2));
                    $attr=array();
                    foreach($a2 as $v)
                    if(ereg('^([^=]*)=["\']?([^"\']*)["\']?$',$v,$a3))
                    $attr[strtoupper($a3[1])]=$a3[2];
                    $this->OpenTag($tag,$attr);		//,$position
                }
            }
        }
    }

    function OpenTag($tag,$attr)		//,$position
    {
        //Balise ouvrante
        if($tag=='STRONG')
        $tag='B';
        if($tag=='B' or $tag=='I' or $tag=='U')
        $this->SetStyle($tag,true);
        if($tag=='A')
        $this->HREF=$attr['HREF'];
        if($tag=='BR')
        $this->Ln(0);
        if($tag=='P'){
            //if($position=='pied'){
            $this->ALIGN=$attr['ALIGN'];
            //}
            //else{
            //$this->newFlowingBlock( 170, 4, '', 'J');
            //}
            $this->Ln(5);
        }
        if($tag=='FONT'){
            if (isset($attr['COLOR']) and $attr['COLOR']!='') {
                $coul=hex2dec($attr['COLOR']);
                $this->SetTextColor($coul['R'],$coul['V'],$coul['B']);
                $this->issetcolor=true;
            }
            if (isset($attr['FACE']) and in_array(strtolower($attr['FACE']), $this->fontlist)) {
                $this->SetFont(strtolower($attr['FACE']));
                $this->issetfont=true;
            }
        }
    }

    function CloseTag($tag,$position)
    {
        //Balise fermante
        if($tag=='STRONG')
        $tag='B';
        if($tag=='B' or $tag=='I' or $tag=='U')
        $this->SetStyle($tag,false);
        if($tag=='A')
        $this->HREF='';
        if($tag=='P'){
            //if($position=='pied'){
            $this->ALIGN='';
            //}
            //else{
            //$this->finishFlowingBlock();
            //}
            //$ptag=1;
        }
        if($tag=='FONT'){
            if ($this->issetcolor==true) {
                $this->SetTextColor(0);
            }
            if ($this->issetfont) {
                $this->SetFont('arial');
                $this->issetfont=false;
            }
        }
    }*/

    /*function SetStyle($tag,$enable)
    {
        //Modifie le style et s�lectionne la police correspondante
        $this->$tag+=($enable ? 1 : -1);
        $style='';
        foreach(array('B','I','U') as $s)
        if($this->$s>0)
        $style.=$s;
        $this->SetFont('',$style);
    }*/

    /*function PutLink($URL,$txt)
    {
        //Place un hyperlien
        $this->SetTextColor(0,0,255);
        $this->SetStyle('U',true);
        $this->Write(5,$txt,$URL);
        $this->SetStyle('U',false);
        $this->SetTextColor(0);
    }
    function WriteText($text)
    {
        $intPosIni = 0;
        $intPosFim = 0;
        if (strpos($text,'<')!==false and strpos($text,'[')!==false)
        {
            if (strpos($text,'<')<strpos($text,'['))
            {
                $this->Write(5,substr($text,0,strpos($text,'<')));
                $intPosIni = strpos($text,'<');
                $intPosFim = strpos($text,'>');
                $this->SetFont('','B');
                $this->Write(5,substr($text,$intPosIni+1,$intPosFim-$intPosIni-1));
                $this->SetFont('','');
                $this->WriteText(substr($text,$intPosFim+1,strlen($text)));
            }
            else
            {
                $this->Write(5,substr($text,0,strpos($text,'[')));
                $intPosIni = strpos($text,'[');
                $intPosFim = strpos($text,']');
                $w=$this->GetStringWidth('a')*($intPosFim-$intPosIni-1);
                $this->Cell($w,$this->FontSize+0.75,substr($text,$intPosIni+1,$intPosFim-$intPosIni-1),1,0,'');
                $this->WriteText(substr($text,$intPosFim+1,strlen($text)));
            }
        }
        else
        {
            if (strpos($text,'<')!==false)
            {
                $this->Write(5,substr($text,0,strpos($text,'<')));
                $intPosIni = strpos($text,'<');
                $intPosFim = strpos($text,'>');
                $this->SetFont('','B');
                $this->WriteText(substr($text,$intPosIni+1,$intPosFim-$intPosIni-1));
                $this->SetFont('','');
                $this->WriteText(substr($text,$intPosFim+1,strlen($text)));
            }
            elseif (strpos($text,'[')!==false)
            {
                $this->Write(5,substr($text,0,strpos($text,'[')));
                $intPosIni = strpos($text,'[');
                $intPosFim = strpos($text,']');
                $w=$this->GetStringWidth('a')*($intPosFim-$intPosIni-1);
                $this->Cell($w,$this->FontSize+0.75,substr($text,$intPosIni+1,$intPosFim-$intPosIni-1),1,0,'');
                $this->WriteText(substr($text,$intPosFim+1,strlen($text)));
            }
            else
            {
                $this->Write(5,$text);
            }

        }
    }*/

        /* En-t�te */
    function Header()
    {

        //$this->Image('../fpdf/images/logo_edc.png',10,3,40);

    }

    function courrier_inscription(){
        $this->SetFont('optima','',11.5);
        $this->SetXY(114,47);
        $date = date('d - Y');
        $month = date('F');
        $month = trad_mois($month);



        //Uniquement les premières années
        $req_etudiant = "SELECT * FROM candidats, inscriptions WHERE candidats.id = inscriptions.id_candidat and annee like '1%'";

        $result_etudiant = mysql_query($req_etudiant);

        while($etudiants = mysql_fetch_assoc($result_etudiant) ){

            $date = str_replace('-',$month,$date);
//Modif Yann => Remonté l'adresse pour que ca puisse ressortir dans le cadre de l'enveloppe
            $this->setxy(112,46);
            $this->Ln(5);




                $civilite=$etudiants['civilite'];
                $nom=$etudiants['nom'];
                $prenom=$etudiants['prenom'];
                $adresse=$etudiants['adresse'];
                $adressesuite=$etudiants['adresse2'];
                $cp=$etudiants['cp'];
                $ville=$etudiants['ville'];
                $pays=$etudiants['pays'];


            $civilite=conv_civilite($civilite);

            $this->SetX(112);
            $this->Multicell(85,4,$civilite." ".$prenom." ".$nom."\n",0,'L');
            $this->SetX(112);
            $this->Multicell(85,4,$adresse."\n",0,'L');
            $this->SetX(112);
            $this->Multicell(85,4,$adressesuite."\n",0,'L');
            $this->SetX(112);
            if ($pays=='FRANCE'){
                $this->Multicell(85,4,$cp." ".$ville."\n",0,'L');
            }
            else{
                $this->Multicell(85,4,$cp." ".$ville." ".$pays."\n",0,'L');
            }

//Modif yann => Inverser la civilité en cas de "Monsieur et madame"
            if($civilite=="Monsieur et Madame"){
                $civilite="Madame, Monsieur";
            }


//Modif Yann => remonter le corps du message
            $this->Ln(6);
            $this->SetX(112);
            $this->Multicell(72,4,"Courbevoie, le ".$date, 0, 'L');
            $this->Ln(12);
            $this->SetX(19);
            $this->Multicell(162,4,html_entity_decode("Objet : Rentr&eacute;e 2009-2010 / Promotion 2014",ENT_QUOTES),0,'L');
            $this->Ln(8);
            $this->SetX(20);
            $texte = "<p>".$civilite." ".$prenom." ".$nom.",</p><p> Je tiens tout d'abord, avec l'ensemble de l'&eacute;quipe p&eacute;dagogique, &agrave; vous adresser ";
            $texte .= "mes plus vives f&eacute;licitations pour votre r&eacute;ussite au concours d'entr&eacute;e &agrave; EDC.</p>";
            $texte .= "<p>Je vous informe que nous aurons le plaisir de vous accueillir &agrave; l'Ecole le <vb>mercredi 2 septembre 2009 &agrave; 14h</vb> ";
            $texte .= "pour une r&eacute;union d'information sur l'organisation g&eacute;n&eacute;rale de la premi&egrave;re ann&eacute;e, r&eacute;union &agrave; laquelle participera le parrain de ";
            $texte .= "votre promotion et o&ugrave; vous seront pr&eacute;sent&eacute;es les valeurs qui animent l'Ecole et qui guideront votre parcours tout au ";
            $texte .= "long de votre scolarit&eacute; &agrave; EDC. Vous voudrez bien noter que votre pr&eacute;sence &agrave; cette r&eacute;union est <vb>OBLIGATOIRE</vb> et que ";
            $texte .= "vous devrez, d&egrave;s votre arriv&eacute;e, <place>vous pr&eacute;senter au service scolarit&eacute;, &agrave; l'entr&eacute;e de l'amphith&eacute;&acirc;tre</place>, pour &eacute;marger.</p>";
            $texte .= "<p>Cette demi-journ&eacute;e de rentr&eacute;e sera suivie les 3 et 4 septembre d'une pr&eacute;sentation d&eacute;taill&eacute;e du programme de 1&egrave;re ann&eacute;e, du cursus ";
            $texte .= "complet de l'Ecole, des associations, de tests linguistiques et d'une d&eacute;couverte des locaux.</p>";
            $texte .= "<p>Du lundi 7 au vendredi 18 septembre 2009 se d&eacute;roulera votre s&eacute;minaire de rentr&eacute;e  dont l'objectif principal est de vous transmettre ";
            $texte .= "les outils m&eacute;thodologiques indispensables &agrave; la r&eacute;ussite de votre scolarit&eacute; &agrave; EDC et &agrave; votre insertion dans le monde professionnel ";
            $texte .= "&agrave; l'issue de celle-ci. A titre indicatif, ce s&eacute;minaire traitera des sujets suivants : </p>";
            $texte .= "<p>- communication interpersonnelle ;</p>";
            $texte .= "<p>- communication &eacute;crite (orthographe, prise de notes, r&eacute;daction d'un compte-rendu, ...) ;</p>";
            $texte .= "<p>- organisation du travail et gestion du temps ;</p>";
            $texte .= "<p>- mise en place du fil rouge \" Projet Professionnel \" ; </p>";
            $texte .= "<p>- perfectionnement linguistique. </p>";
            $texte .= "<p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p>";
            $texte .= "<p>Les enseignements g&eacute;n&eacute;raux commenceront &agrave; partir du lundi 21 septembre 2009.</p>";
            $texte .= "<p>Afin de faciliter nos futurs &eacute;changes avec vos parents, vous voudrez bien transmettre leur(s) adresse(s) &eacute;lectronique(s) ";
            $texte .= "&agrave; Sophie BEHAR (<a>sophie.behar@edcparis.edu</a>), secr&eacute;taire p&eacute;dagogique de 1&egrave;re ann&eacute;e. Nous organiserons une r&eacute;union ";
            $texte .= "d'information &agrave; leur intention dans le courant du mois de novembre.</p>";
            $texte .= "<p>Dans l'attente du plaisir de vous accueillir &agrave; EDC, je me tiens &agrave; votre enti&egrave;re disposition pour toute information ";
            $texte .= "compl&eacute;mentaire.</p><p>Je vous prie de croire $civilite, &agrave; l'assurance de mes meilleures salutations.</p>";

            $this->WriteTag(0,6,html_entity_decode($texte,ENT_QUOTES),0,"J",0);

            //$this->Multicell(163,4,html_entity_decode($texte,ENT_QUOTES),0,'J');
            $this->Ln(2);
            //$this->SetX(20);
            //$this->Image('../../images/signe_mg.jpg',105,$this->GetY(),35);

//Modif Yann => Remonter la signature
            $this->SetXY(105,$this->GetY()+30);
            $signature="Marika GARREL\nResponsable P&eacute;dagogique 1&egrave;re ann&eacute;e";
            $this->Multicell(162,4,html_entity_decode($signature,ENT_QUOTES),0,'L');

            $this->setxy(20,180);

            $coordonnees = "<p1>Coordonn&eacute;es de l'&eacute;quipe p&eacute;dagogique en charge de la 1&egrave;re ann&eacute;e :</p1>";
            $coordonnees .= "<p1><vb>Direction P&eacute;dagogique</vb> : Marika GARREL</p1>";
            $coordonnees .= "<p1><vb>Email</vb> : <a>marika.garrel@edcparis.edu</a></p1>";
            $coordonnees .= "<p1><vb>TEL</vb> : 01 46 93 03 82 (ligne directe) - 01 46 93 02 70 (standard)</p1>";
            $coordonnees .= "<p1><vb>FAX</vb> : 01 46 93 02 81</p1>";
            $coordonnees .= "<p1><vb>Scr&eacute;tariat P&eacute;dagogique</vb> : Sophie BEHAR</p1>";
            $coordonnees .= "<p1><vb>Email</vb> : <a>sophie.behar@edcparis.edu</a></p1>";
            $coordonnees .= "<p1><vb>TEL</vb> : 01 46 93 03 83 (ligne directe) - 01 46 93 02 70 (standard)</p1>";
            $coordonnees .= "<p1><vb>FAX</vb> : 01 46 93 02 81</p1>";

            $this->WriteTag(130,6,html_entity_decode($coordonnees,ENT_QUOTES),1,"L",0);

            $this->AddPage();
        }

    }

        /** Pied de page */
    function Footer()
    {

        //$this->SetXY(20,-10);
        //$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }

}
?>