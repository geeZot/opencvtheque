<?php
/*
appel de la classe ; 
$cnx = new $actiondata ();

Les fonctions disponibles dans la classe
	connect()
	requeteSelect ($sql,$typeIndex ='assoc')
	requeteData ($sql)
	requeteMulti ($sql)
	securite($chaine)
	last_id()
	debug($error)
	deconnect()
	select_liste($table,$id,$text)
	select_data($champs,$tables,$conditions="")
	update_data($champs,$tables,$valeurs,$conditions="",$suffixe_req="")

 */

if(!isset($serveur)||empty($serveur))
{
    require_once('../conf/_connect.php');
}

define ('MYSQLI_SERVEUR',$serveur);
define ('MYSQLI_LOGIN',$user);
define ('MYSQLI_PASSWORD',$password);
define ('MYSQLI_BDD',$bdd);
//define ('MYSQLI_BDD',$bdd);


class actionsdata
{



public function __construct($host=MYSQLI_SERVEUR,$login=MYSQLI_LOGIN, $password=MYSQLI_PASSWORD, $bdd=MYSQLI_BDD)
{
 $this->host=$host;
 $this->login=$login;
 $this->password=$password;
 $this->base=$bdd;
}

// connection à une base de donnée
function connect()
{
        $connect = mysqli_connect ($this->host, $this->login, $this->password,$this->base);


        if (mysqli_connect_errno())
            die ("Echec de la connexion : ". mysqli_connect_error());

        $this->connect = $connect;


        $this->connect->set_charset("utf8");

//        mysqli_set_charset ($conn,"utf8") ;


/* Modification du jeu de résultats en utf8 */
/*if (!$mysqli->set_charset("utf8")) {
    printf("Erreur lors du chargement du jeu de caractères utf8 : %s\n", $mysqli->error);
} else {
    printf("Jeu de caractères courant : %s\n", $mysqli->character_set_name());
}
        */

// test à faire mysql_select_db
        // a voir pconnect


}

// execute une requete SQL
function requeteSelect ($sql,$typeIndex ='assoc')
{
/*
$sql : requete SQL
$typeIndex : format spécifique
*/

try {

//echo $sql."<br>";
            //$sql = $this->connect->real_escape_string($sql);
            $qid=$this->connect->query($sql);
//print_r($qid);
//echo "<br>";
	if ($qid->num_rows==0 )
	{
            //echo "*";
        	$rows=0;
        }
        else
        {
            //echo "+";
        	switch ($typeIndex)
                {
                	case 'assoc' : 
				while ($row = $qid->fetch_assoc())
				{
					 $rows[] = $row;
				}
                         break;
                         case 'row' : 
				while ($row = $qid->fetch_row())
				{
					$rows[] = $row;
				}
                         break;
                        }

         } 


                return $rows;

	}
	
	catch (requeteSelect $error) 
	{
	        $this->debug($error);
	        return false;
	}

}




// Ajout ou mise à jour de la base de données
function requeteData ($sql)
{
/*
sql : fonction SQL
*/

	if (!$sql)
	{
            //echo "*";
        	return false;
        }
        else
        {
            try {
        	$qid=$this->connect->query($sql);

            }
            catch (requeteData $error){
	        $qid=$this->debug($error);
	        return false;
            }


         }
        return array($qid);


//        $erreur = $sql."<br />".$this->connect->error;

}




// Ajout ou mise à jour de la base de données
function requeteMulti ($sql)
{
/*
sql : toutes les requetes SQL
*/
	
	if (!$sql) return;

	try
	{
		$this->connect->multi_query($sql);      
     
            do 
	    {
                if ($result = $this->connect->store_result()) 
		{
                    while ($row = $result->fetch_row()) 
		    {
			echo $row[0]."<br>";
                    }
                    $result->free();
                }

            } while ($this->connect->next_result());

        return true;

	}
	catch (requeteMulti $error) 
	{
	        $this->debug($error);
	        return false;
	}

}


// securise valeur entrante
function securite($string)
{
     return $this->connect->real_escape_string($string);
}



// Récupère dernier enregistrement
public function last_id()
{
       return $this->connect->insert_id;
}


// deconnection
function deconnect()
{
        $this->connect->close();
}


function debug($error)
{

	$msg  = "Error Code : ".$error->getCode()."<br />";
	$msg .= "Error Message : ".$error->getMessage()."<br />";
	$msg .= "Strack Trace : ".nl2br($error->getTraceAsString())."<br />";

        die ($msg);
}

function select_liste($table,$id,$text,$where="",$orderby='true')
{
/*
  $table = nom de la table
  $id : champ ID de la table 
  $text : le champ texte qui sera affiché
*/
        $sql="SELECT $id,$text FROM ".$table." ";
        if($where !="")  $sql.="WHERE $where ";
        if($orderby=='true') $sql.= "ORDER BY $text";

        $data=$this->requeteSelect($sql);

        foreach($data as $row)
        {
                $rows[$row[$id]] = $row[$text];
        }

       return $rows;
}

/*
  $table = nom de la table 1
  $id1 : champ ID de la table 1
  $text1 : le champ texte qui sera affiché
  $join1 : cle etrangere de la table 1
  $table2 = nom de la table 2
  $text2 : le champ texte qui sera affiché
  $where : clause where (AND)
*/
function select_liste_double($table1,$id1,$text1,$join1,$table2,$id2,$text2="",$join2="",$where="")
{
/*
  $table = nom de la table
  $id : champ ID de la table (on veut impérativement avoir le nom de l'id donnee)
  $text : le champ texte qui sera affiché
*/
        $sql="SELECT a.".$id1." as idtable1,$text1 FROM ".$table1." a, $table2 b ";
        if($join2!="")
            $sql.=" WHERE a.".$join1."= b.".$join2." ";
        else
            $sql.=" WHERE a.".$join1."= b.".$id2." ";
        if($where !="")  $sql.="AND $where ";
        $sql.= "ORDER BY $text1";

        //echo $sql."<br/>";

        $data=$this->requeteSelect($sql);

        foreach($data as $row)
        {
                $rows[$row['idtable1']] = $row[$text1];
        }

        return $rows;
}


public function select_data($champs,$tables,$conditions="")
{
// $champs : noms des champs
// $tables : noms des tables
// conditions : les conditions WHERE

    $index_nom_champ = 'nom';
    $index_database = 'database';

	try {


            $req="SELECT ";
            //var_dump($champs);
	    foreach($champs as $fieldset){
                
                if(array_key_exists('database', $fieldset)){
                    $database='ok';
                }
                else{
                    $database='pasok';
                }
                foreach($fieldset as $champ){

                    if($database=='ok' ){
                        $val_database = $champ[$index_database];
                        $database="";
                    }
                    else{
                        $val_database = "";
                        $database="";
                    }

                    if(is_array($champ) && $champ[$index_nom_champ]!='' && $val_database!='no'){
                        $req .= "".$champ[$index_nom_champ].",";
                    }
                }
        }
        $req = substr($req,0,-1);

        $req .= " FROM ";
        foreach($tables as $table){
            $req .= "".$table.",";
        }
        $req = substr($req,0,-1);
        if($conditions!=""){
            $req .= " WHERE ";


            foreach($conditions as $liste){

                if(!is_array($liste)){
                    $req .= " ".$liste." ";
                }
                else{
                    foreach($liste as $condition => $valeur){
                        $req .= "".$condition.$valeur."";
                    }
                }

            }

        }
//echo "###".$req."###";

$cnx=new actionsdata();
$cnx->connect();
$result=$cnx->requeteSelect($req);
$cnx->deconnect();

return $result;

}
catch (actionsdataException $error) 
{
        $this->debug($error);
        return false;
}

}

    public function insert_data(){


    }

/*
 * MISE A JOUR DES DONNEES
 *
 *
 */


public function update_data($champs,$tables,$valeurs,$conditions="",$suffixe_req="")
{
/*
$champs
$tables
$valeurs
$conditions
$suffixe_req
*/


        $nb_tables = count($tables);
        //echo $nb_tables."<br/>";
        foreach($tables as $table){
            $tab_requetes['table']['req_'.$table]="UPDATE ".$table." SET ";
        }

        foreach($champs as $fieldset){
            foreach($fieldset as $champ){

                if(key_exists('database', $fieldset)){
                    $database = $champ['database'];
                } else {
                    $database = '';
                }

                if (key_exists('formatdate', $fieldset)){
                    $formatdate = $champ['formatdate'];
                } else {
                    $formatdate = '';
                }

                if(is_array($champ) && $champ['nom'] != '' && $database != 'no'){
                    //echo strrchr($champ['nom'], '_')."<br/>";
                    $int_champ=str_replace('_','-',$champ['nom']);
                    if($formatdate == 'yes'){
                        $valeurs[$int_champ]=strftime("%F",strtotime(str_replace('/','-',$valeurs[$int_champ])));
                    }

                    if($champ['rules']=='numeric'){
                        $valeurs[$int_champ]=$valeurs[$int_champ];
                    }

                    $tab_requetes['table']['req_'.$suffixe_req.strrchr($champ['nom'], '_')]=
                    $tab_requetes['table']['req_'.$suffixe_req.strrchr($champ['nom'], '_')].$champ['nom']." = '".$valeurs[$int_champ]."',";
                }
            }
        }
//        var_dump($tables);

        foreach($tables as $table){
            $tab_requetes['table']['req_'.$table]=substr($tab_requetes['table']['req_'.$table], 0, -1).
            " WHERE ";
            foreach($conditions as $liste){
                if(!is_array($liste)){
                    $tab_requetes['table']['req_'.$table] = $tab_requetes['table']['req_'.$table]." ".$liste." ";
                }
                else{
                    foreach($liste as $condition => $valeur){
                        $tab_requetes['table']['req_'.$table] = $tab_requetes['table']['req_'.$table].$condition.$valeur;
                    }
                }
            }
            $tab_requetes['table']['req_'.$table] = $tab_requetes['table']['req_'.$table].";";
        }

        foreach($tab_requetes['table'] as $requete)
	{
            $req .= $requete;
	}
echo $req;
	$cnxTMP=new actionsdata();
	$cnxTMP->connect();


	if($nb_tables == 1)			// une requete
	{
		$result=$cnxTMP->requeteSelect($req);
		return TRUE;
	}
	else				//multi requete
	{
		$result=$cnxTMP->requeteMulti($req);
		return TRUE;
	}

    }

  


    public function __destruct()
    {

    }

    
        
    
function pagination($cnx,$sql,$start='',$page,$limit)
{
/* definit la pagination d'une liste
$cnx : envoie la connexion
$sql : requete SQL sans la partie LIMIT
$start : numéro page de départ
$page : position du numero de page
$limit : nombre de lignes à afficher en mémoire
*/

	if (is_null($page))    $page=1;
	if (is_null($limit))    $limit=10;

	$rows=$cnx->requeteSelect($sql);

	$count = count($rows);

    if( $count >0 ) 
	{
            $total_pages = ceil($count/$limit);
    }
    else 
	{
            $total_pages = 0;
    }

    if ($page > $total_pages) $page=$total_pages;

    $start = $limit*$page - $limit; // do not put $limit*($page - 1)

    if ($start<0) $start = 0;

	$result=array(
			"start" => $start,
			"page" => $page,
			"total_pages" => $total_pages,		
			"count" => $count,		
			"limit" => $limit		
		);

	return $result;
}


function pagination2($cnx,$start,$page,$limit)
{
/* definit la pagination d'une liste
$cnx : envoie la connexion
$sql : requete SQL sans la partie LIMIT
$start : numéro page de départ
$page : position du numero de page
$limit : nombre de lignes à afficher en mémoire
*/

	if (is_null($page))    $page=1;
	if (is_null($limit))    $limit=10;

	$rows=$cnx->requeteSelect("SELECT FOUND_ROWS() as numrows");

	$count = intval($rows['numrows']);

    if( $count >0 )
	{
            $total_pages = ceil($count/$limit);
    }
    else
	{
            $total_pages = 0;
    }

    if ($page > $total_pages) $page=$total_pages;

    $start = $limit*$page - $limit; // do not put $limit*($page - 1)

    if ($start<0) $start = 0;

	$result=array(
			"start" => $start,
			"page" => $page,
			"total_pages" => $total_pages,
			"count" => $count,
			"limit" => $limit
		);

	return $result;
}
      
    
}


?>