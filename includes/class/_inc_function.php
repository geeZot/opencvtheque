<?php


/*
 *
 * Fonction de traduction des mois anglais --> francais
 * FLE le 29/05/09
 *
 */

function trad_mois($month){
    switch($month){
				case "January" : $monthtrad = 'Janvier';break;
				case "February" : $monthtrad = 'F&eacute;vrier';break;
				case "March" : $monthtrad = 'Mars';break;
				case "April" : $monthtrad = 'Avril';break;
				case "May" : $monthtrad = 'Mai';break;
				case "June" : $monthtrad = 'Juin';break;
				case "July" : $monthtrad = 'Juillet';break;
				case "August" : $monthtrad = 'Ao&ucirc;t';break;
				case "September" : $monthtrad = 'Septembre';break;
				case "October" : $monthtrad = 'Octobre';break;
				case "November" : $monthtrad = 'Novembre';break;
				case "December" : $monthtrad = 'D&eacute;cembre';break;
			}
    return $monthtrad;
}

/*
 *
 * Fonction de conversion pour la civilite
 *
 */

function conv_civilite($civilite,$param=''){
    if($param==''){
        switch ($civilite){
            case 1:
                $civilite='Mademoiselle';
                break;
            case 2:
                $civilite='Madame';
                break;
            case 3:
                $civilite='Monsieur';
                break;
            case 4:
                $civilite='Monsieur et Madame';
                break;
        }
    }
    else{
        $tab_civilite=array_values($param);
        $civilite=$tab_civilite[$civilite-1];
    }
    
    return $civilite;
}

function conv_date($date_origine){
    $date_modif =  date('d/m/Y',strtotime($date_origine));
    return $date_modif;
}

function conv_date_en($date_origine,$separation="/"){
	$date_modif=explode ($separation,$date_origine);	
	return $date_modif[2]."-".$date_modif[1]."-".$date_modif[0];
}

function conv_mois($chiffre){

	    switch($chiffre){
				case "01" : $moisEnClair = 'janvier';break;
				case "02" : $moisEnClair = 'f&eacute;vrier';break;
				case "03" : $moisEnClair = 'mars';break;
				case "04" : $moisEnClair = 'avril';break;
				case "05" : $moisEnClair = 'mai';break;
				case "06" : $moisEnClair = 'juin';break;
				case "07" : $moisEnClair = 'juillet';break;
				case "08" : $moisEnClair = 'ao&ucirc;t';break;
				case "09" : $moisEnClair = 'septembre';break;
				case "10" : $moisEnClair = 'octobre';break;
				case "11" : $moisEnClair = 'novembre';break;
				case "12" : $moisEnClair = 'd&eacute;cembre';break;
			}
    return $moisEnClair;
}
function rpHash($value) {
    $hash = 5381;
    $value = strtoupper($value);
    for($i = 0; $i < strlen($value); $i++) {
        $hash = (($hash << 5) + $hash) + ord(substr($value, $i));
    }
    return $hash;
}


//compatible avec la classe mysqli
function genSelectToXml($name,$table_result,$num=1,$xml=false,$width="auto")
{
    $str = ($xml) ? "<?xml version=\"1.0\"?>" : "";
    $str.="<select style='width:".$width."px;' name='$name' id='$name'>";
    $str.="<option value='0'>Faire un choix</option>";
    
    foreach($table_result as $cle => $ligne)
    {
      //si l'on recoit un couple de valeur (par exemple tiré d'une requete)
      if(is_array($ligne))
      {
         //sinon si l'on reçoit une de valeur simplement
         $str .= "<option value='".($ligne['0'])."'";
         if($ligne['0'] == $num) $str.= " selected ";
         $str .= ">".$ligne['1']."</option>";
      }
      else
      {
      //sinon si l'on reçoit une de valeur simplement
      $str .= "<option value='".($cle+1)."'";
      if(($cle+1) == $num) $str.= " selected ";
      $str .= ">$ligne</option>";
      }
    }
    $str.="</select>";

    return $str;
}



/*
 * Fonction qui permet de générer un select avec des optgroup
 * basé sur un fichier xml
 * indiquer uniquement le chemin du fichier xml
 * val_required =  si la liste est obligatoire ou non mettre yes sinon no par défaut
 */
function select_generation($xmvalue,$val_required='no',$isfile=TRUE){
	if ($isfile && file_exists($xmvalue)){
            $selectsecteur = simplexml_load_file($xmvalue);
        }
	else {
            $xmvalue=utf8_encode($xmvalue);
            $selectsecteur = simplexml_load_string($xmvalue);
        }

    $listevaleurs = $selectsecteur->getName();
    $htcode= "<".$listevaleurs." id=".$selectsecteur['id']." name=".$selectsecteur['name']." require=".$val_required.">\n";
    foreach($selectsecteur->children() as $element){

        if ($element->getName()=='optgroup'){
            $htcode.= "<".$element->getName()." ".$element[0]->attributes()->getName()."='".$element->attributes()."'>\n";
            foreach($element->children() as $selement){
                $htcode.= "<".$selement->getName()." ".$selement[0]->attributes()->getName()."='".$selement->attributes()."'>".$selement."</".$selement->getName().">\n";
            }
            $htcode.= "</".$element->getName().">\n";
        }
        else{
            $htcode.= "<".$element->getName()." ".$element[0]->attributes()->getName()."='".$element->attributes()."'>".$element."</".$element->getName().">\n";
        }

    }
    $htcode.= "</".$listevaleurs.">\n";
    return $htcode;
}


   
function conv_operator($searchfield,$operator,$searchstring){

    switch ($operator) {

        case 'bw':
            //bw - begins with ( LIKE val% )
            $req = " AND ".$searchfield." LIKE '".$searchstring."%'";
            break;
       case 'bn':
            //bn - begins not with ( NOT LIKE val% )
            $req = " AND ".$searchfield." NOT LIKE '".$searchstring."%'";
            break;
       case 'ni':
            //ni - not in ( NOT LIKE %val% )
            $req = " AND ".$searchfield." NOT LIKE '%".$searchstring."%'";
            break;
       case 'en':
            //ni - end not ( NOT LIKE %val )
            $req = " AND ".$searchfield." NOT LIKE '%".$searchstring."'";
            break;
        case 'eq':
            //eq - equal ( = )
            $req = " AND ".$searchfield."=".$searchstring;
            break;
        case 'ne':
            //ne - not equal ( <> )
            $req = " AND ".$searchfield."!=".$searchstring;
            break;
        case 'lt':
            //lt - little ( < )
            $req = " AND ".$searchfield."<".$searchstring;
            break;
        case 'le':
            //le - little or equal ( <= )
            $req = " AND ".$searchfield."<=".$searchstring;
            break;
        case 'gt':
            //gt - greater ( > )
            $req = " AND ".$searchfield.">".$searchstring;
            break;
        case 'ge':
            //ge - greater or equal ( >= )
            $req = " AND ".$searchfield.">=".$searchstring;
            break;
        case 'ew':
            //ew - ends with (LIKE %val )
            $req = " AND ".$searchfield." LIKE '%".$searchstring."'";
            break;
        case 'cn':
            //cn - contain (LIKE %val% )
            $req = " AND ".$searchfield." LIKE '%".$searchstring."%'";
            break;
        case 'nc':
            //cn - not contain (NOT LIKE %val% )
            $req = " AND ".$searchfield." NOT LIKE '%".$searchstring."%'";
            break;

    }
//echo $req;
return $req;

}

/*
 * Fonction qui permet de retailler une image en conservant son ratio
 * $largeur = largeur actuelle de l'image
 * $hauteur = hauteur actuelle de l'image
 * $new_largeur = nouvelle largeur souhaitée
 * $new_hauteur = nouvelle hauteur souhaitée
 */

function ratio_image($largeur,$hauteur,$new_largeur,$new_hauteur){

   if($largeur > $new_largeur || $hauteur > $new_hauteur){
        $ratio_l=$largeur / $new_largeur;
        $ratio_h=$hauteur / $new_hauteur;

        if($ratio_h>=$ratio_l){
            if($hauteur > $new_hauteur){
                $new_size['largeur']=$largeur/$ratio_h;
                $new_size['hauteur']=$hauteur/$ratio_h;
            }
        }
        else{
            if($largeur > $new_largeur){
                $new_size['largeur']=$largeur/$ratio_l;
                $new_size['hauteur']=$hauteur/$ratio_l;
            }

        }

    }
    else{
        $new_size['largeur']=$largeur;
        $new_size['hauteur']=$hauteur;
    }

return $new_size;

}

/*
 * Fonction qui effectue le traitement nécessaire aux photo conversion
 * modification de type etc...
 * $filename = nom réel du fichier
 * $tmpname =  nom temporaire sur le serveur
 * $filesize =  poid de l'image
 * $filetype = type mime
 * $new_hauteur  = nouvelle hauteur désirée
 * $new_largeur = nouvelle largeur désirée
 *
 */

function upload_photo($fileName,$tmpName,$fileSize,$fileType,$new_hauteur="",$new_largeur="",$returnArray=true,$urlDestination=""){


    list($largeur, $hauteur)=getimagesize($tmpName);

    //Taille maxi à récupérer

    if($new_hauteur==""){
       $new_hauteur=$hauteur;
    }

    if($new_largeur==""){
       $new_largeur=$largeur;
    }

    $nouvelle_taille=ratio_image($largeur,$hauteur,$new_largeur,$new_hauteur);

    $destination = imagecreatetruecolor($nouvelle_taille['largeur'], $nouvelle_taille['hauteur']);

    switch(getExtension($fileType)){
        case 'jpeg':
            imagecopyresampled($destination, imagecreatefromjpeg($tmpName), 0, 0, 0, 0, $nouvelle_taille['largeur'], $nouvelle_taille['hauteur'],$largeur,$hauteur);
            break;
        case 'png':
            imagecopyresampled($destination, imagecreatefrompng($tmpName), 0, 0, 0, 0, $nouvelle_taille['largeur'], $nouvelle_taille['hauteur'], $largeur, $hauteur);
            break;
        case 'gif':
            imagecopyresampled($destination, imagecreatefromgif($tmpName), 0, 0, 0, 0, $nouvelle_taille['largeur'], $nouvelle_taille['hauteur'], $largeur, $hauteur);
            break;
        case 'bmp':
            imagecopyresampled($destination, imagecreatefrombmp($tmpName), 0, 0, 0, 0, $nouvelle_taille['largeur'], $nouvelle_taille['hauteur'], $largeur, $hauteur);
            break;
    }
    
    if($returnArray)
    {
        imagejpeg($destination,$tmpName."_new.tmp",100);
        $fileSize = filesize($tmpName."_new.tmp");
        $fp      = fopen($tmpName."_new.tmp", 'r');
        $content = fread($fp, filesize($tmpName."_new.tmp"));
        $content = addslashes($content);
        $fileType = 'image/jpeg';
        $fileName = substr($fileName,0,-3).'jpg';
        fclose($fp);
        imagedestroy($destination);

        if(!get_magic_quotes_gpc())
        {
            $fileName = addslashes($fileName);
        }

        return array('fileName'=>$fileName,'fileSize'=>$fileSize,'fileType'=>$fileType,'content'=>$content);
    }
    else
    {
       // chmod($urlDestination, 0777);
        return imagejpeg($destination,$urlDestination,100);
    }
}


//utile pour fichiers image , pdf et txt etc.
function getExtension($type)
{
    $exp = explode("/",$type);
    return $exp[1];
}
/*
 * Fonction de recherche pour l'autocompletion
 * $input =  est la valeur récupérée du champ input
 * $table =  est la table dans laquelle on effectue la recherche
 * fields = tableau des colonnes sur lesquelles on fait la recherche
 * $return_lab = tableau des valeurs que l'on souhaite afficher dans la liste
 * $return_val = valeur à retourner
 */

function autocompletion($input,$table,$fields,$return_lab,$return_val,$id='id'){
    //$input = $_GET["q"];
    $data = array();
    // query your DataBase here looking for a match to $input
    $col_field="";
        foreach($fields as $field){
            $col_field .= $field.",";
        }
    $col_field=substr($col_field,0,-1);

    $req_autocomplete = "SELECT * FROM $table WHERE CONCAT($col_field) LIKE '%$input%' ORDER BY $return_val LIMIT 10";
    //echo $req_autocomplete;
    $res_autocomplete = mysql_query($req_autocomplete);

    while ($row = mysql_fetch_assoc($res_autocomplete)) {
        $json = array();
        $label="";
        foreach($return_lab as $champ){
            $label .= $row[$champ]." ";
        }
        $json['label'] = $label;
        $json['value'] = $row[$return_val];
        $json['both'] = $row[$id];
        /*foreach($return_val as $champ){
            $json[$champ]=$row[$champ];
        }*/

        $data[] = $json;
    }
    header("Content-type: application/json");
    echo json_encode($data);
}

//function qui supprime les accents
function suppr_accent($chaine){
    
    $str_accents=array('À','Á','Â','Ã','Ä','Å','Ç','È','É','Ê','Ë','Ì','Í','Î','Ï','Ò','Ó','Ô','Õ','Ö','Ù','Ú','Û','Ü','Ý','à','á','â','ã','ä','å','ç','è','é','ê','ë','ì','í','î','ï','ð','ò','ó','ô','õ','ö','ù','ú','û','ü','ý','ÿ');
    $str_clean=array('A','A','A','A','A','A','C','E','E','E','E','I','I','I','I','O','O','O','O','O','U','U','U','U','Y','a','a','a','a','a','a','c','e','e','e','e','i','i','i','i','o','o','o','o','o','o','u','u','u','u','y','y');

    $new_chaine=str_replace($str_accents,$str_clean,$chaine);

    return $new_chaine;
}


function premLettreMajuscule($str)
{
    $firstLetter = ucfirst(strtolower($str));
    return $firstLetter;
}

//retourne l'annee academique en cours
function anneeScolaire()
{
    $mounth = intval(date("m"));

    //distinction entre deux années scolaires par rapport aux mois de l'année
    if($mounth>=1 && $mounth<8)
    {
      $annee = (date("Y")-1)."/".date("Y");
    }
    else if($mounth>=8 && $mounth<=12)
    {
        $annee = date("Y")."/".(date("Y")+1);
    }
    return $annee;
}


/*Fonction de connexion ldap */

function check_ldapuser($username, $password, $ldap_serveur,$ldap_base_dn,$ldap_user_suffixe)
{

//@eval("include 'acces/connexion.conf.php';");
	//global $global_config;
	//echo ("global : ".$global_config."<br />");
	$user = $username.$ldap_user_suffixe;
	//echo ($user."<br />");
	$connect = @ldap_connect($ldap_serveur)
		or die("Echec de la connexion au serveur LDAP.");
	@ldap_set_option($connect, LDAP_OPT_PROTOCOL_VERSION, 3); // on passe le LDAP en version 3, necessaire pour travailler avec le AD
	@ldap_set_option($connect, LDAP_OPT_REFERRALS, 0);

	$ldapbind = @ldap_bind($connect, $user, $password);

    if ($ldapbind) {
        //echo "Connexion LDAP r&eacute;ussie";
        //echo "#".$username ." ".$password."#";
        return 0;
    } else {
        //echo "Connexion LDAP &eacute;chou&eacute;e";
        return 1;
    }

/*	if ($bind == FALSE) return 1;
	else if($bind == TRUE) return 0;*/

}



function get_ldapinfo($username,$password,$ldap_serveur,$ldap_base_dn,$ldap_user_suffixe)
{
//	global $global_conf;
//@eval("include 'acces/connexion.conf.php';");

	$filter="(|(sn=$username*)(sAMAccountName=$username*))";
	$justthese = array("cn", "sn", "givenname", "mail", "streetAddress", "st", "postOfficeBox", "postalCode", "co", "l");

	$user = $username.$ldap_user_suffixe;

	$connect = @ldap_connect($ldap_serveur)
			or die("Echec de la connexion au serveur LDAP.");
		@ldap_set_option($connect, LDAP_OPT_PROTOCOL_VERSION, 3); // on passe le LDAP en version 3, necessaire pour travailler avec le AD
		@ldap_set_option($connect, LDAP_OPT_REFERRALS, 0);
	$bind = @ldap_bind($connect, $user, $password);

	$sr = @ldap_search($connect, $ldap_base_dn, $filter, $justthese);

	$info_ldap = @ldap_get_entries($connect, $sr);

	//return $info_etudiant["count"];//." entres trouves.\n";
	return $info_ldap;
}

function tableautochaine($tabtoconvert){

    if(is_array($tabtoconvert)==true){
        $chaine='';

        foreach($tabtoconvert as $ligne){
            $chaine .= $ligne.", ";
        }

        return substr($chaine,0,-2);
    }
    

}

function tableautovaleur($tabkeys,$tabvaleurs){
    if((is_array($tabkeys)==true) && (is_array($tabkeys)==true)){
        $tabok=array();

        foreach($tabkeys as $key){
            $tabok[]= $tabvaleurs[$key];
        }

        return $tabok;
    }
}

function add_cr($str_in_cr){
    $str_out_cr=str_replace(chr(10), "<br/>", $str_in_cr);
    return $str_out_cr;
}


function get_localisation($id_city,$param){

    if($id_city==''){
        echo 'id_city ne peut être vide';
        exit;
    }

                $sql_localisation = "SELECT name_city,name_state,name_country,name_province,cp,";
                $sql_localisation .= "id_city as id_ville FROM ".$param["view"]["localisation"];
                $sql_localisation .= " WHERE ".$param["view"]["localisation"].".id_city=".$id_city;
                //echo $sql_localisation;

                $cnx_localisation = new actionsdata();
                $cnx_localisation->connect();

                $data_localisation=$cnx_localisation->requeteSelect ($sql_localisation);
                $cp=explode("-", $data_localisation[0]['cp']);
                if(!empty($cp[0])){
                    $data_localisation[0]['cp']=$cp[0];
                }
                //print_r($data_localisation);

                return $data_localisation;

                $cnx_localisation->deconnect();

}

function get_partenaire($searchelement,$searchstring,$contact,$param,$ask_type='id',$returninfo='mix'){

    $cnx= new actionsdata();
    $cnx->connect();

    if($ask_type=='id'){
        $req_partenaire = "SELECT * FROM ". $param["view"]["partenaires"];
        $req_partenaire .= " WHERE ".$searchelement." = ".$searchstring;
        $req_partenaire .= " AND (proprietaire_contact=".$contact." OR proprietaire_entreprise=".$contact.")";
    }
    else{
        $req_partenaire = "SELECT * FROM ". $param["view"]["partenaires"];
        $req_partenaire .= " WHERE ".$searchelement." LIKE '".$searchstring."%'";
        $req_partenaire .= " AND (proprietaire_contact=".$contact." OR proprietaire_entreprise=".$contact.")";
        $req_partenaire .= " ORDER BY raison_sociale_entreprise LIMIT 10";
    }

    $res_partenaire = $cnx->requeteSelect($req_partenaire);

//        $result = mysql_query( $SQL ) or die("Could not execute query.".mysql_error());
/*        $responce->page = $page;
        $responce->total = $total_pages;
        $responce->records = $count;*/
        $i=0;


        $responce=array();
        //"###".count($res_partenaire[0])."###";
        if(count($res_partenaire[0])>0){
        foreach($res_partenaire as $row) {

            if($row['id_contact_partenaire']!= null){
                $id_partenaire=$row['id_contact_partenaire']; // Contact physique
            }
            else {
                $id_partenaire=$row['id']; // Entreprise
            }

            if($searchelement=='nom_contact'){
                $label=$row['nom_contact']." ".$row['prenom_contact']." ".$row['raison_sociale_entreprise'];
                $value=$row['nom_contact'];
/*                $id_city_contact=;
                $id_city_entreprise=;*/
            }
            else{
                $label=$row['raison_sociale_entreprise']." ".$row['nom_contact']." ".$row['prenom_contact'];
                $value=$row['raison_sociale_entreprise'];

            }


            switch($returninfo){
                case 'mix':
                    if(!$row['tel_contact'] || $row['tel_contact'] == NULL){
                        $tel=$row['tel'];
                    }
                    else{
                        $tel=$row['tel_contact'];
                    }
                    $gsm=$row['gsm_contact'];
                    if(!$row['fax_contact'] || $row['fax_contact'] == NULL){
                        $fax=$row['fax'];
                    }
                    else{
                        $fax=$row['fax_contact'];
                    }
                    if(!$row['email_contact'] || $row['email_contact'] == 'null'){
                        $email=$row['email'];
                    }
                    else{
                        $email=$row['email_contact'];
                    }
                        $id_city_partenaire=$row['id_city_entreprise'];
                        $adresse1=$row['adresse'];
                        $adresse2=$row['adresse2'];
                        $cp=$row['cp'];
                        $ville=$row['ville'];

                    break;
                case 'nom_contact':
                    $tel=$row['tel_contact'];
                    $gsm=$row['gsm_contact'];
                    $fax=$row['fax_contact'];
                    $email=$row['email_contact'];
                    $adresse1=$row['adresse1_contact'];
                    $adresse2=$row['adresse2_contact'];
                    $cp=$row['cp_contact'];
                    $ville=$row['ville_contact'];
                    break;
                case 'raison_sociale_entreprise':
                    $tel=$row['tel'];
                    $gsm='';
                    $fax=$row['fax'];
                    $email=$row['email'];
                    $adresse1=$row['adresse'];
                    $adresse2=$row['adresse2'];
                    $cp=$row['cp'];
                    $ville=$row['ville'];
                    break;


            }


            array_push($responce,array("id"=>$id_partenaire,"label"=>$label,"value"=>$value,"both"=>array(
                    "id_contact_partenaire"=>$row['id_contact_partenaire'],
                    "id_entreprise_partenaire"=>$row['id'],
                    "civilite_contact_partenaire"=>$row['civilite_contact'],
                    "nom_contact_partenaire"=>$row['nom_contact'],
                    "raison_sociale_entreprise_partenaire"=>$row['raison_sociale_entreprise'],
                    "prenom_contact_partenaire"=>$row['prenom_contact'],
                    "fonction_contact_partenaire"=>$row['fonction_contact'],
                    "id_city_contact_partenaire"=>$row['id_city_contact'],
                    "id_city_entreprise_partenaire"=>$row['id_city_entreprise'],
                    "id_city_partenaire"=>$id_city_partenaire,
                    "description_partenaire"=>$row['description'],
                    "id_effectif_partenaire"=>$row['ideffectif'],
                    "id_secteur_activite_partenaire"=>$row['secteur_activite_entreprise'],
                    "tel_partenaire"=>$tel,
                    "gsm_partenaire"=>$gsm,
                    "fax_partenaire"=>$fax,
                    "email_partenaire"=>$email,
                    "adresse1_partenaire"=>$adresse1,
                    "adresse2_partenaire"=>$adresse2,
                    "cp_partenaire"=>$cp,
                    "ville_partenaire"=>$ville
                    )));
        }
        
        }
        $cnx->deconnect();
        return $responce;
}


function creer_entreprise_partenaire($id_contact,$info_entreprise,$table){
    $cnx= new actionsdata();
    $cnx->connect();

    $req_new_entreprise_partenaire  = "INSERT INTO ".$table." (";
    $req_new_entreprise_partenaire .= "raison_sociale_entreprise, secteur_activite_entreprise, ";
    $req_new_entreprise_partenaire .= "ideffectif, description, tel,fax,email,adresse, adresse2,";
    $req_new_entreprise_partenaire .= "id_city_entreprise,created_on,id_contact";
    $req_new_entreprise_partenaire .= ") VALUES (";
    $req_new_entreprise_partenaire .= "'".$info_entreprise['raison_sociale_entreprise_partenaire']."',";
    $req_new_entreprise_partenaire .= "'".$info_entreprise['id_secteur_activite_partenaire']."', ";
    $req_new_entreprise_partenaire .= "'".$info_entreprise['id_effectif_partenaire']."', ";
    $req_new_entreprise_partenaire .= "'".$info_entreprise['description_partenaire']."', ";
    $req_new_entreprise_partenaire .= "'".$info_entreprise['tel_partenaire']."', ";
    $req_new_entreprise_partenaire .= "'".$info_entreprise['fax_partenaire']."', ";
    $req_new_entreprise_partenaire .= "'".$info_entreprise['email_partenaire']."', ";
    $req_new_entreprise_partenaire .= "'".$info_entreprise['adresse1_partenaire']."', ";
    $req_new_entreprise_partenaire .= "'".$info_entreprise['adresse2_partenaire']."', ";
    $req_new_entreprise_partenaire .= "'".$info_entreprise['id_city_partenaire']."', ";
    $req_new_entreprise_partenaire .= "'".time()."', ";
    $req_new_entreprise_partenaire .= "'".$id_contact."' ";
    $req_new_entreprise_partenaire .= ")";
    $res_new_entreprise_partenaire=$cnx->requeteData($req_new_entreprise_partenaire);
    $id_new_entreprise_partenaire=$cnx->last_id();

    $cnx->deconnect();
    return $id_new_entreprise_partenaire;
}

function creer_contact_partenaire($id_contact,$info_contact,$id_entreprise_partenaire,$table){
    $cnx= new actionsdata();
    $cnx->connect();

    $req_new_contact_partenaire = "INSERT INTO ".$table." (";
    $req_new_contact_partenaire .= "id_contact, civilite_contact, prenom_contact, nom_contact, ";
    $req_new_contact_partenaire .= "fonction_contact, fax_contact, gsm_contact, tel_contact, ";
    $req_new_contact_partenaire .= "email_contact, adresse1_contact, adresse2_contact, ";
    $req_new_contact_partenaire .= "id_city_contact, id_entreprise,created_on) VALUES (";
    $req_new_contact_partenaire .= "'".$id_contact."', ";
    $req_new_contact_partenaire .= "'".$info_contact['civilite_contact_partenaire']."', ";
    $req_new_contact_partenaire .= "'".$info_contact['prenom_contact_partenaire']."', ";
    $req_new_contact_partenaire .= "'".$info_contact['nom_contact_partenaire']."', ";
    $req_new_contact_partenaire .= "'".$info_contact['fonction_contact_partenaire']."', ";
    $req_new_contact_partenaire .= "'".$info_contact['fax_partenaire']."', ";
    $req_new_contact_partenaire .= "'".$info_contact['gsm_partenaire']."', ";
    $req_new_contact_partenaire .= "'".$info_contact['tel_partenaire']."', ";
    $req_new_contact_partenaire .= "'".$info_contact['email_partenaire']."', ";
    $req_new_contact_partenaire .= "'".$info_contact['adresse1_partenaire']."', ";
    $req_new_contact_partenaire .= "'".$info_contact['adresse2_partenaire']."', ";
    $req_new_contact_partenaire .= "'".$info_contact['id_city_partenaire']."', ";
    $req_new_contact_partenaire .= "'".$id_entreprise_partenaire."', ";
    $req_new_contact_partenaire .= time().") ";
    $res_new_contact_partenaire=$cnx->requeteData($req_new_contact_partenaire);
    $id_new_contact_partenaire=$cnx->last_id();
    $cnx->deconnect();
    return $id_new_contact_partenaire;
}

function get_secteur($id_secteur,$langue){
    $liste_secteurs_xml = simplexml_load_file('includes/listes/sel-secteurs-'.$langue.'.xml');
    $xml=$liste_secteurs_xml->xpath('/select/optgroup/option');

    foreach($xml as $row){
    if($row[0]['value']==$id_secteur){
        $secteur_activite=$row[0];
        break;
        }
    }
    
    return strval($secteur_activite);
}

?>