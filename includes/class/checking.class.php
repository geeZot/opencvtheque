<?php
class Checking
{
	/**
	 * Vérifie si une valeur est vide / existe
	 *
	 * @param mixed $values
	 * @return bool
	 */
	public static function notEmpty($values)
	{
		// Si $values n'est pas un array
		if (!is_array($values)) {
			$value = trim($values);

			if (!empty($value) && isset($value))
			{
				return true;
			}

			return false;
		}

		foreach ($values as $value)
		{
			$value = trim($value);

			if (empty($value) || !isset($values)) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Vérifie si une valeur est égale à une autre
	 *
	 * @param array $values
	 * @return bool
	 */
	public static function isEqual($values)
	{
		if ($values[0] !== $values[1]) {
			return false;
		}

		return true;
	}

	/**
	 * Vérifie si une valeur est égale à zéro
	 *
	 * @param string $value
	 * @return bool
	 */
	public static function notZero($value)
	{
		//if ($value === 0) {
                //les variables get et post renvoi des chaine de caractères
                //donc le test de type sera toujours faut
                if ($value == 0) {
			return false;
		}

		return true;
	}

	/**
	 * Vérifie que le mail est valide
	 *
	 * @param string $mail
	 * @return bool
	 */
	public static function isValidMail($mail)
	{
		if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) {
			return false;
		}

		return true;
	}

	/**
	 * Vérifie que ce soit bien un entier
	 *
	 * @param int $int
	 * @return bool
	 */
	public static function isInt($int)
	{
		if (!filter_var($int, FILTER_VALIDATE_INT)) {
			return false;
		}

		return true;
	}

	/**
	 * Vérifie que l'image envoyé est bien un "jpg"
	 *
	 * @params $index string
	 * @return bool
	 */
	public static function isJpg($index)
	{
		if (empty($_FILES[$index]['name'])) {
			return true;
		}

		$path_parts = pathinfo($_FILES[$index]['name']);

		if ($path_parts['extension'] != 'jpg') {
			return false;
		}

		return true;
	}

        /**
	 * Vérifie que la valeur ne depasse pas minValue
	 *
	 * @param array $values
	 * @return bool
	 */
	public static function isMin($values)
	{
		if ($values[0] > $values[1]) {
			return false;
		}
		return true;
	}

         /**
         * Vérifie que la valeur ne depasse pas maxValue
         *
         * @param array $values
         * @return bool
         */
	public static function isMax($values)
	{
		if ($values[0] < $values[1]) {
			return false;
		}
		return true;
	}
}
?>
