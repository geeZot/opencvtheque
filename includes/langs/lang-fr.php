<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * Statut partenaire/entreprise
 *
 */

//Datepicker
$param["datepicker"]["dayNames"]="['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi']";
$param["datepicker"]["dayNamesMin"]="['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa']";
$param["datepicker"]["dayNamesShort"]="['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam']";

$param["datepicker"]["monthNames"]="['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre']";
$param["datepicker"]["monthNamesShort"]="['Jan', 'Fév', 'Mar', 'Avr', 'Mai', 'Jui', 'Jui', 'Aou', 'Sep', 'Oct', 'Nov', 'Déc']";

$param["action"]["add"] = "Ajouter";
$param["action"]["update"] = "Modifier";
$param["action"]["view"] = "Afficher";
$param["action"]["duplicate"] = "Dupliquer";
$param["action"]["delete"] = "Supprimer";
$param["action"]["close"] = "Fermer";
$param["action"]["open"] = "Ouvrir";
$param["action"]["cancel"]="Annuler";
$param["action"]["create"]="Créer";

//Statut partenaire/entreprise
$param["statepartenaire"][0]="Actif";
$param["statepartenaire"][1]="Inactif";
$param["statepartenaire"][2]="Supprimé";

//liste annee
$param["annee"]["annee1"]="bac+1";
$param["annee"]["annee2"]="bac+2";
$param["annee"]["annee3"]="bac+3";
$param["annee"]["annee4"]="bac+4";
$param["annee"]["annee5"]="bac+5";
$param["annee"]["diplome"]="Jeune diplômé";

//Type de contrat
$param["contrat"]["cdd"]="CDD";
$param["contrat"]["cdi"]="CDI";
$param["contrat"]["1emp"]="1er emploi";
$param["contrat"]["stage"]="Stage";
$param["contrat"]["vie"]="VIE";
$param["contrat"]["cesure"]="Césure";

//Reponse oui/non
$param["reponse"][1]="Oui";
$param["reponse"][2]="Non";

//Civilite
$param["civilite_short"]["M"]="M.";
$param["civilite_short"]["Mme"]="Mme";
$param["civilite_short"]["Melle"]="Melle";

$param["civilite"][1]="Monsieur";
$param["civilite"][2]="Madame";
$param["civilite"][3]="Mademoiselle";

//Ecran login
$param["login"]["identifiant"]="Identifiant";
$param["login"]["mdp"]="Mot de passe";
$param["login"]["rememberme"]="Se souvenir de moi";
$param["login"]["creecompte"]="Pas encore de compte ?";
$param["login"]["connect"]="Se connecter";

//Création compte
$param["typecontact"]["type-candidat"]="Un candidat"; //1
$param["typecontact"]["type-entreprise"]="Une entreprise"; //2
$param["typecontact"]["type-cabinet"]="Un cabinet"; //3

$param["creation"]["titre"]="Créer un compte";
$param["creation"]["whois"]="Vous êtes";
$param["creation"]["societe"]="Société";
$param["creation"]["raison_sociale"]="Raison sociale";
$param["creation"]["secteurs"]="Secteurs d'activité";
$param["creation"]["web"]="Site web";
$param["creation"]["contact"]="Utilisateur";
$param["creation"]["civilite"]="Civilité";
$param["creation"]["nom-contact"]="Nom";
$param["creation"]["prenom-contact"]="Prénom";
$param["creation"]["fonction-contact"]="Fonction";
$param["creation"]["adresse1-contact"]="Adresse";
$param["creation"]["adresse2-contact"]="Complément";
$param["creation"]["cp-contact"]="Code postal";
$param["creation"]["ville-contact"]="Ville";
$param["creation"]["pays-contact"]="Pays";
$param["creation"]["tel-contact"]="Tél.";
$param["creation"]["fax-contact"]="Fax";
$param["creation"]["emailmdp"]="E-mail et Mot de passe";
$param["creation"]["email-contact"]="E-mail";
$param["creation"]["mdp-contact"]="Mot de passe";
$param["creation"]["confmdp-contact"]="Confirmez votre mot de passe";
$param["creation"]["err-mdp"]="Les mots de passe ne sont pas identiques";
$param["creation"]["captcha-contact"]="Captcha";
$param["creation"]["capchange-contact"]="Cliquez pour changer";

//Menu

$param["menu"]["lister"]="lister";

//Titre liste
$param["title"]["listecv"]="Liste CV";

//Gestion CV
$param["gestioncv"]["id"]="ID";
$param["gestioncv"]["namefile"]="Nom fichier";
$param["gestioncv"]["Type"]="Type";
$param["gestioncv"]["date"]="Date upload";
$param["gestioncv"]["title"]="Ajouter un CV";
$param["gestioncv"]["show"]="Cliquer pour voir le cv";
$param["gestioncv"]["default"]="CV Actif";
$param["gestioncv"]["cvcheck"]="CV Actif:CV Inactif";
$param["gestioncv"]["commentaires"]="Commentaires";

//Gestion profil candidat
$param["gestionprofil"]["title"]="Mon profil";
$param["gestionprofil"]["etatcivil"]="Etat civil";
$param["gestionprofil"]["civilite-contact"]="Civilité";
$param["gestionprofil"]["nom-contact"]="Nom";
$param["gestionprofil"]["prenom-contact"]="Prénom";
$param["gestionprofil"]["date-naissance-candidat"]="Date de naissance";
$param["gestionprofil"]["contact"]="Me contacter";
$param["gestionprofil"]["email-contact"]="E-mail";
$param["gestionprofil"]["adresse1-contact"]="Adresse";
$param["gestionprofil"]["adresse2-contact"]="Complément";
$param["gestionprofil"]["cp-contact"]="Code postal";
$param["gestionprofil"]["ville-contact"]="Ville";
$param["gestionprofil"]["pays-contact"]="Pays";
$param["gestionprofil"]["tel-contact"]="Tél.";
$param["gestionprofil"]["gsm-contact"]="Gsm";
$param["gestionprofil"]["divers"]="Divers";
$param["gestionprofil"]["site-web-candidat"]="Site web";
$param["gestionprofil"]["niveau-etudes-candidat"]="Niveau d'études";
$param["gestionprofil"]["date-permis-candidat"]="Date permis de conduire";
$param["gestionprofil"]["situation-candidat"]="Confidentialité";
$param["gestionprofil"]["situation-anonyme"]="Masquer état civil";
$param["gestionprofil"]["situation-visible"]="Afficher état civil";

//Gestion profil entreprise
$param["gestionprofilent"]["contact"]="Fiche contact";
$param["gestionprofilent"]["title"]="Mon profil";
$param["gestionprofilent"]["etatcivil"]="Etat civil";
$param["gestionprofilent"]["civilite-contact"]="Civilité";
$param["gestionprofilent"]["nom-contact"]="Nom";
$param["gestionprofilent"]["prenom-contact"]="Prénom";
$param["gestionprofilent"]["email-contact"]="E-mail";
$param["gestionprofilent"]["adresse1-contact"]="Adresse";
$param["gestionprofilent"]["adresse2-contact"]="Complément";
$param["gestionprofilent"]["cp-contact"]="Code postal";
$param["gestionprofilent"]["ville-contact"]="Ville";
$param["gestionprofilent"]["pays-contact"]="Pays";
$param["gestionprofilent"]["tel-contact"]="Tél.";
$param["gestionprofilent"]["fax-contact"]="Fax";
$param["gestionprofilent"]["gsm-contact"]="Gsm";
$param["gestionprofilent"]["logo"]="Logo entreprise";
$param["gestionprofilent"]["fonction-contact"]="Fonction";
$param["gestionprofilent"]["entreprise"]="Fiche entreprise";
$param["gestionprofilent"]["raison-sociale-entreprise"]="Raison sociale";
$param["gestionprofilent"]["description-entreprise"]="Description";
$param["gestionprofilent"]["secteur-activite-entreprise"]="Secteur";
$param["gestionprofilent"]["site-web-entreprise"]="Site web";


//Gestion offre entreprise
$param["gestionoffreent"]["offre"]="Détail offre";
$param["gestionoffreent"]["candidat"]="Profil candidat";
$param["gestionoffreent"]["contact"]="Personne à contacter";
$param["gestionoffreent"]["reference-offre"]="Référence";
$param["gestionoffreent"]["intitule-offre"]="Intitulé";
$param["gestionoffreent"]["region-offre"]="Région";
$param["gestionoffreent"]["txt-lieu-offre"]="Ville";
$param["gestionoffreent"]["type-offre"]="Type";
$param["gestionoffreent"]["type-contrat-offre"]="Contrat";
$param["gestionoffreent"]["date-debut-offre"]="Date de début";
$param["gestionoffreent"]["niveau-qualification-offre"]="Niveau d'études";
$param["gestionoffreent"]["secteur-offre"]="Secteur entreprise";
$param["gestionoffreent"]["description-offre"]="Description";
$param["gestionoffreent"]["profil-candidat-offre"]="Profil du candidat";
$param["gestionoffreent"]["salaire-min-offre"]="Salaire minimum";
$param["gestionoffreent"]["salaire-max-offre"]="Salaire maximum";
$param["gestionoffreent"]["nom-contact"]="Entreprise";
$param["gestionoffreent"]["contact-offre"]="Contact";

$param["gestionoffreent"]["duplicate"]="Dupliquer une offre";
$param["gestionoffreent"]["update"]="Modification de l'offre";
$param["gestionoffreent"]["add"]="Ajouter une offre";


//Messages d'erreurs
$param["erreur"]["acces"]="Vous n'êtes pas autorisé à accéder à cette page !!!";
$param["erreur"]["email"]="Un compte existe déjà avec cette adresse mail";
$param["erreur"]["captcha"]="Le captcha a été corrompu";
$param["erreur"]["login"]="Identifiant et/ou mot de passe incorrect(s)";
$param["erreur"]["extension"]="Extension non autorisée";
$param["erreur"]["nblimit"]="Nombre maximal de CV atteint";


?>
