<?php
/* 
 * Ce fichier est à inclure dans chaque fichier de requete
 * du type req-xxxxxx.php
 * 
 */
//session_start();
//echo "<br/>req : ".__DIR__."<br/>";
//require_once (__dir__.'/_connect.php');
require_once ("../includes/class/_inc_function.php");

//récupère la langue du navigateur
if (!isset($Langue)) {
    $Langue = explode(',',$_SERVER['HTTP_ACCEPT_LANGUAGE']);
    $Langue = strtolower(substr(chop($Langue[0]),0,2));
}

require_once ("../includes/langs/lang-".$Langue.".php");
require_once ("../conf/include_tables.php");
require_once ('../includes/class/listes.class.php');
require_once ("../includes/class/formulaire.class.php");
require_once ("../includes/class/requetes.class.php");
require_once ("../includes/class/mail.class.php");
require_once ("../includes/class/securite.class.php");

/*
 * Déconnecte l'utilisateur si la session a expirée
 */
if(!isset($_SESSION)){
//        echo "isset";
    header("Location: .") ;
}

if(empty($_SESSION))
{
//echo "empty";
  //destruction de toutes les variable de sessions
  //session_unset() ;
  //destruction de la session
  //session_destroy() ;

      //echo "<meta http-equiv='refresh' content='2;URL=http://".$installpath."/'>";
}


//récupère la connection à la base et les différents paramètres

$securite=new securite();

//echo getenv('SCRIPT_FILENAME');
//vérifie que la requête vient bien du même site

/*$ref=getenv('HTTP_REFERER');
if($ref!="http://".$installpath."/"){
    echo $param["erreur"]["acces"];
    exit;
}*/

?>
