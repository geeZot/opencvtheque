<?php
/*
* redirecteur.php?Created 1 févr. 2011 at 13:13:59 by flenoble under Ocv-NG
* $Id$
*/
define( 'ABSPATH', dirname(__FILE__) . '/' );
require_once (ABSPATH.'conf/_connect.php');
$destination_origine = "";

if(isset($_GET['dest'])){
    $destination_origine = $_GET['dest'];    
}

if(isset($_GET['page'])){
    //sert à récupérer une page lors d'un passage d'une page au travers
    //de la session
    $page = $_GET['page'];
    //echo "page : ".$page."###";
}

$destination = str_replace("_","/", $destination_origine);
session_start();

if(isset($_SESSION['contact'])) {
    $contact = $_SESSION['contact'];
}

$securite = new securite();
$cnx = new actionsdata();
$cnx->connect();

//"###".var_dump($_SESSION['page'])."###";

include_once ABSPATH.$destination.'.php';
