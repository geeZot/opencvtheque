<?php

$securite=new securite;
$id_offre=$securite->verif_GetPost($_POST['id']);
$id_contact=$_SESSION['contact'];
$niveauetude = array();
$langueexigee = array();
$languesouhaitee = array();

?>
<script type="text/javascript">
$(document).ready(function() {
    $('input:submit, button, .button', '').button();
    $("#bt_close").click(function(){
        $('#formulairePerso').dialog('close')
    });
});
</script>
<?php


    $cnx = new actionsdata();
    $cnx->connect();

            /*$req_select_offre = "SELECT id_offre,id_city,description_offre,region_offre,lieu_offre,salaire_max_offre,avantage, ";
            $req_select_offre .="date_debut_offre,intitule_offre,type_offre,idduree,iddeplacement,complement,despossible, ";
            $req_select_offre .= "secteur_activite_entreprise,raison_sociale_entreprise,description,adresse,ideffectif, ";
            $req_select_offre .= "entreprise_anonym,tel,fax,email, ";
            $req_select_offre .= "b.CIVILITE_CONTACT,b.nom_contact,prenom_contact,fonction_contact, ";
            $req_select_offre .= "b.id as id_entreprise_partenaire,b.id_contact_partenaire as id_contact_partenaire ";
            $req_select_offre .=" FROM ".$param["table"]["offre"]." a,".$param["view"]["partenaires"]." b ";
            $req_select_offre .= "WHERE a.id_offre=".$id." ";
            $req_select_offre .= "AND (a.id_entreprise_partenaire = b.id OR a.id_contact_partenaire = b.id_contact_partenaire)";
*/
            $req_select_offre = "SELECT id_offre,id_city,description_offre,region_offre,lieu_offre,complement,";
            $req_select_offre .= "date_debut_offre,intitule_offre,type_offre,idduree,iddeplacement,profil_candidat_offre,";
            $req_select_offre .= "avantage,despossible,entreprise_anonym,id_entreprise_partenaire,";
            $req_select_offre .= "salaire_max_offre,id_contact_partenaire,offre_partenaire,id_contact,";
            $req_select_offre .= "created_on ";

            $req_select_offre .=" FROM ".$param["table"]["offre"]." a ";//,".$param["view"]["partenaires"]." b ";
            $req_select_offre .= "WHERE a.id_offre=".$id_offre." ";

	    $res_select_offre=$cnx->requeteSelect ($req_select_offre);
            //echo $req_select_offre."<br/>";

            if($res_select_offre[0]['id_city']!=0){

                $data_localisation=get_localisation($res_select_offre[0]['id_city'],$param);

                $city=$data_localisation[0]['name_city'];
                $province=$data_localisation[0]['name_province'];
                $state=$data_localisation[0]['name_state'];
                $country=$data_localisation[0]['name_country'];

            }
            else{
                $city=$res_select_offre[0]['lieu_offre'];
                $province='';
                $state=$res_select_offre[0]['region_offre'];
                $country='';
            }

            //echo "op : ".$res_select_offre[0]['id_entreprise_partenaire']."<br/>";
            if($res_select_offre[0]['offre_partenaire']==1) {
                $info_partenaire=get_partenaire('id',$res_select_offre[0]['id_entreprise_partenaire'], $res_select_offre[0]['id_contact'], $param);
            }


            if($res_select_offre[0]['entreprise_anonym']==0){
                $raison_sociale=$info_partenaire[0][both]['raison_sociale_entreprise_partenaire'];
                $civilite_contact=$info_partenaire[0][both]['civilite_contact_partenaire'];
                $nom_contact=$info_partenaire[0][both]['nom_contact_partenaire'];
                $prenom_contact=$info_partenaire[0][both]['prenom_contact_partenaire'];
                $fonction_contact=$info_partenaire[0][both]['fonction_contact_partenaire'];
                $tel= $info_partenaire[0][both]['tel_contact_partenaire'];
		$fax = $info_partenaire[0][both]['fax_contact_partenaire'];
		$email = $info_partenaire[0][both]['email_contact_partenaire'];
                $id_effectif_partenaire = $info_partenaire[0][both]['id_effectif_partenaire'];
                //print_r($info_partenaire);
            }
            else{
                $req_get_info_contact = "SELECT civilite_contact,prenom_contact,nom_contact,";
                $req_get_info_contact .= "fonction_contact,fax_contact,tel_contact,email_contact,";
                $req_get_info_contact .= "raison_sociale_entreprise,secteur_activite_entreprise,";
                $req_get_info_contact .= "site_web_entreprise,logo_entreprise,description_entreprise FROM ";
                $req_get_info_contact .= $param["table"]["entreprise"].",".$param["table"]["contact"]." ";
                $req_get_info_contact .= "WHERE ".$param["table"]["contact"].".id_contact=".$res_select_offre[0]['id_contact']." AND ";
                $req_get_info_contact .= $param["table"]["entreprise"].".id_contact=";
                $req_get_info_contact .= $param["table"]["contact"].".id_contact";

                //echo $req_get_info_contact;

                $res_get_info_contact = $cnx->requeteSelect ($req_get_info_contact);
                $raison_sociale = $res_get_info_contact[0]['raison_sociale_entreprise'];
                if($res_select_offre[0]['offre_partenaire']==1) {
                    $raison_sociale .= " (".$info_partenaire[0]['both']['raison_sociale_entreprise_partenaire'].")";
                    $description_entreprise = $info_partenaire[0]['both']['description_partenaire'];
                }
                else
                {
                    $description_entreprise = $res_get_info_contact[0]['description_entreprise'];
                }
                $civilite_contact = $res_get_info_contact[0]['civilite_contact'];
                $nom_contact = $res_get_info_contact[0]['nom_contact'];
                $prenom_contact = $res_get_info_contact[0]['prenom_contact'];
                $fonction_contact = $res_get_info_contact[0]['fonction_contact'];
                $tel= $res_get_info_contact[0]['tel_contact'];
		$fax = $res_get_info_contact[0]['fax_contact'];
		$email = $res_get_info_contact[0]['email_contact'];
                $id_effectif_partenaire = '';
            }



            $req_type_offre="SELECT nom FROM ".$param["table"]["type_offre"]." WHERE id=".$res_select_offre[0]['type_offre'];
            $type_offre=$cnx->requeteSelect ($req_type_offre);

            $secteur_activite_entreprise=get_secteur($info_partenaire[0]['both']['id_secteur_activite_partenaire'], $Langue);


	 $data_offre = array(
	 			'id_offre'=> $res_select_offre[0]['id_offre'],
	 			'intitule_offre'=> $res_select_offre[0]['intitule_offre'],
	 			'description_offre'=> $res_select_offre[0]['description_offre'],
    	 			'city_offre'=> $city,
	 			'province_offre'=> $province,
                                'state_offre'=>$state,
                                'country_offre'=>$country,
	 			'typeoffre'=> $type_offre[0]['nom'],
	 			'idduree'=> $res_select_offre[0]['idduree'],
	 			'iddeplacement'=> $res_select_offre[0]['iddeplacement'],
	 			'salaire_max_offre'=> $res_select_offre[0]['salaire_max_offre'],
	 			'avantage'=> $res_select_offre[0]['avantage'],
	 			'complement'=> $res_select_offre[0]['complement'],
	 	 		'despossible'=> $res_select_offre[0]['despossible'],
	 			'date_depot'=> $res_select_offre[0]['created_on'],
	 			'date_debut_offre'=> $res_select_offre[0]['date_debut_offre'],
				'secteur_activite_entreprise'=> $secteur_activite_entreprise,
	 			'entreprise'=> $raison_sociale,
				'description'=> $description_entreprise,
				//'adresse1'=> $res_select_offre[0]['adresse'],
				'tel'=> $tel,
				'fax'=> $fax,
				'email'=> $email,
				'civilite_contact'=> $civilite_contact,
				'nom_contact'=> $nom_contact,
				'prenom_contact'=> $prenom_contact,
				'fonction_contact'=> $fonction_contact,
	 			'entreprise_anonym'=> $res_select_offre[0]['entreprise_anonym'],
	 			'id_contact'=> $res_select_offre[0]['id_contact'],
	 			'id_entreprise'=> $res_select_offre[0]['id_entreprise_partenaire'],
	 			'ideffectif'=> $id_effectif_partenaire
                                //'id'=> $res_select_offre[0]['id_offre']
	 
	 );
    
	 $date_debut_offre=conv_date ($data_offre['date_debut_offre']);

// partie checkbox (plusieurs choix possible)
		$req_niveau_etude = "SELECT idniveauetude, idlangueexigee, idlanguesouhaitee ";
                $req_niveau_etude .=" FROM cv_offre_details ";
		$req_niveau_etude .= "WHERE idoffre=".$id_offre." ";
		$req_niveau_etude .= "ORDER BY idniveauetude, idlangueexigee, idlanguesouhaitee ";

                //echo $req_niveau_etude;


                $res_niveau_etude=$cnx->requeteSelect ($req_niveau_etude);
		if ($res_niveau_etude)
		{
			foreach ($res_niveau_etude as $row)
			{
				if ($row['idniveauetude'])   $niveauetude[] = $row['idniveauetude'];
				if ($row['idlangueexigee'])  $langueexigee[] = $row['idlangueexigee'];
				if ($row['idlanguesouhaitee'])  $languesouhaitee[] = $row['idlanguesouhaitee'];
			}
		} 



$result="<div id='infos_cles'><div id='infos_cles_contenu'>";

$result.= "<h2 id=''>Infos clés</h2>";
$result.="<dl><dt>Entreprise : </dt><dd>".$data_offre['entreprise']."</dd></dl>";
$result.="<dl><dt>Region : </dt><dd>".$data_offre['city_offre'].", ";
$result.= $data_offre['province_offre'].", ";
$result.= $data_offre['state_offre'].", ";
$result.= $data_offre['country_offre']."</dd></dl>";
$result.= "<dl><dt>Secteur : </dt>";
$result.= "<dd>".$data_offre['secteur_activite_entreprise']."</dd></dl>";
$result.= "<dl><dt>Type de poste : </dt>";
$result.= "<dd>".$data_offre['typeoffre']."</dd></dl>";
$result.= "<dl><dt>Date début : </dt>";
$result.= "<dd>".$date_debut_offre."</dd></dl>";
$result.= "<dl><dt>Expérience : </dt>";
$value=$cnx->select_liste('cfg_niveau_etude',"id","nom");
$result.= "<dd>".tableautochaine(tableautovaleur($niveauetude,$value))."</dd>";
$result.="</dl></div></div>";


$result .= "<h2 id='titre_page'>Détails offre</h2><div id='detailoffre'>";


/*$chaine = strstr($data_offre['description_offre'],"existants");
echo $chaine[10].ord($chaine[10])."<br>";
echo $chaine[11].ord($chaine[11])."<br>";
echo $chaine[12].ord($chaine[12])."<br>";
echo $chaine[13].ord($chaine[13])."<br>";
echo $chaine[14].ord($chaine[14])."<br>";*/



$form = new formulaire ();

/*$result .= $form->fieldset_new();
$value = $cnx->select_liste('cfg_type_offre',"id","nom");
$result .= $form->affiche_radio('Type',$value,$data_offre['type_offre']);
$result .= $form->fieldset_end();*/

//-----
$result .= $form->fieldset_new("Entreprise");
//affiche_text('Entreprise',$data_offre['entreprise'],$class='',$style='',$id='',$force=false,$classlabel="formlabel")
$result .= $form->affiche_text('Entreprise',$data_offre['entreprise'],'offredescription','','',false,'offrelabel');

/*$fichier="../includes/listes/sel-secteurs-fr.xml";
$result .= $form->affiche_select('Secteur d\'activité','',$fichier,$data_offre['secteur_activite_entreprise']);*/

$value=$cnx->select_liste('cfg_effectif',"id","nom");
$result .= $form->affiche_select('Effectif',$value,'',$data_offre['ideffectif'],'offredescription','offrelabel');

$result .= $form->affiche_text('Description de l\'entreprise',$data_offre['description'],'offredescription','','',false,'offrelonglabel');
$result .= $form->fieldset_end();
//-----
$result .= $form->fieldset_new("Mission");
$result .= $form->affiche_text('Intitulé du poste',$data_offre['intitule_offre'],'offredescription','mot','',false,'offrelonglabel');
$result .= $form->affiche_text('Description de la mission',$data_offre['description_offre'],'offredescription','','',false,'offrelonglabel');
/*$result .= $form->affiche_text('Date début',$date_debut_offre,'offredescription','date','',false,'offrelabel');*/

$value=$cnx->select_liste('cfg_duree',"id","nom");
$result .= $form->affiche_radio('Durée',$value,$data_offre['idduree'],'offredescription','offrelabel');
/*$result .= $form->affiche_text('Ville',$data_offre['city_offre'],'offredescription','plusmots','',false,'offrelabel');
$result .= $form->affiche_text('Région',$data_offre['state_offre'],'offredescription','plusmots','',false,'offrelabel');*/

$value= $param["reponse"];
$result .= $form->affiche_radio('Déplacements éventuels',$value,$data_offre['iddeplacement'],'offredescription','offrelonglabel');
$result .= $form->affiche_text('Rémunération proposée',$data_offre['salaire_max_offre'],'offredescription','','',false,'offrelonglabel');
$result .= $form->affiche_text('Avantages',$data_offre['avantage'],'offredescription','','',false,'offrelabel');
$result .= $form->fieldset_end();
//-----
$result .= $form->fieldset_new("Profil candidat");
$value=$cnx->select_liste('cfg_niveau_etude',"id","nom");
$result .= $form->affiche_checkbox('Niveau d\'études',$value,$niveauetude,'offredescription','offrelonglabel');
$value=$cnx->select_liste('cfg_langue',"id","nom");
$result .= $form->affiche_checkbox('Langue(s) exigée(s)',$value,$langueexigee,'offredescription','offrelabel');
$result .= $form->affiche_checkbox('Langue(s) souhaitée(s)',$value,$languesouhaitee,'offredescription','offrelabel');

$result .= $form->affiche_text('Complément profil',$data_offre['complement'],'offredescription','','',false,'offrelonglabel');
$result .= $form->fieldset_end();

//-----
$result .= $form->fieldset_new("Contact");
$value= $param["civilite"];

$result .= $form->affiche_text('Civilite',  conv_civilite($data_offre['civilite_contact'],$param['civilite']),'offredescription','mot','',false,'offrelabel');
$result .= $form->affiche_text('Nom',$data_offre['nom_contact'],'offredescription','mot','',false,'offrelabel');
$result .= $form->affiche_text('Prénom',$data_offre['prenom_contact'],'offredescription','mot','',false,'offrelabel');
$result .= $form->affiche_text('Fonction',$data_offre['fonction_contact'],'offredescription','mot','',false,'offrelabel');

$result .= $form->affiche_text('Tel',$data_offre['tel'],'offredescription','','',false,'offrelabel');
$result .= $form->affiche_text('Fax',$data_offre['fax'],'offredescription','','',false,'offrelabel');
$result .= $form->affiche_text('Email',$data_offre['email'],'offredescription','','',false,'offrelabel');
$result .= $form->fieldset_end();

$result .= "</div>";

/*$result .= $form->form_init('gestionoffreent','#','POST','formulaire');*/
$result .= $form->creer_bouton($param["action"]["close"],'bt_close','button','button');
$result .= $form->form_end();
/**/

if($_SESSION["type"]==1){
    $req_update_offre="UPDATE ".$param["table"]["offre"]." SET nb_consultation_offre=nb_consultation_offre+1, updated_on=".time()." ";
    $req_update_offre .= "WHERE id_offre=".$id_offre;
    $res_update_offre=$cnx->requeteData ($req_update_offre);

    $req_maj_stats = "INSERT ".$param["table"]["statistiques"]." (ID_OFFRE,ID_CONTACT,";
    $req_maj_stats .= "adresse_ip,referrer,created_on) VALUES ('".$id_offre."',";
    $req_maj_stats .= "'".$id_contact."','".$_SESSION['remote']."',";
    $req_maj_stats .= "'".$_SERVER['HTTP_REFERER']."','".date("Y-m-d H:i:s")."')";
    $res_maj_stats = $cnx->requeteData ($req_maj_stats);
}

echo $result;

?>
