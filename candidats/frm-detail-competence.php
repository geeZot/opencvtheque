<?php
/*
* req-liste-competences-trad.php Created 1 mars 2011 at 13:18:06 by flenoble under Ocv-NG
* $Id$
*/
?>
<script type="text/javascript">
<?php
require_once ("candidats/js/fonction_detail_competences.js");
require_once ("includes/js/form-fonctions.js");


?>
</script>
<?php
$clean_get = $securite->clean_tab($_GET);

//echo "get : ".$_GET['id']."<br/>";
$result='<h1>Détail : '.$clean_get["title"]."</h1>";
$form = new formulaire();
$liste = new listes();
$langues =  $liste->select_liste($param["table"]["langues"], 'id_langue', 'nom_langue');

//if(substr($clean_get["id"], 0,3) != 'ns-') {
$req_get_competence_langue = "SELECT id_competence,id_langue,";
$req_get_competence_langue .= "nom_langue FROM ".$param["table"]["langues"]." ";
$req_get_competence_langue .= "LEFT OUTER JOIN ".$param["table"]["acquerircompetence"]." ON ";
$req_get_competence_langue .= $param["table"]["acquerircompetence"].".id_langue_competence = ";
$req_get_competence_langue .= $param["table"]["langues"].".id_langue ";
$req_get_competence_langue .= "WHERE (id_contact = '".$contact."' OR ";
$req_get_competence_langue .= "id_contact IS NULL) AND (";
$req_get_competence_langue .= "id_competence = '".$clean_get["id"]."' OR ";
$req_get_competence_langue .= "id_competence IS NULL)";


    /*
     SELECT id_langue, nom_langue, description_competence, id_contact, id_competence
FROM cv_langue
LEFT OUTER JOIN cv_acquerir_competence ON cv_acquerir_competence.id_langue_competence = cv_langue.id_langue
WHERE (
ID_contact =3
OR id_contact IS NULL
)
AND (
id_competence =8
OR id_competence IS NULL 
     */

    //echo $req_get_competence_langue;

    $res_get_competence_langue = $cnx-> requeteSelect($req_get_competence_langue);
    $result .= $form->fieldset_new('Langues');
    $result .= "<div id='listelangue'>";
    $result .= "<ol id='selectable'>";

    foreach($res_get_competence_langue as $langue){

        if($langue["id_competence"]==$clean_get["id"]){
            $result .= "<li id='".$contact."-".$langue["id_langue"]."-".$clean_get["id"]."'>".$langue["nom_langue"]." *</li>";
        } else{
            $result .= "<li id='ns-".$contact."-".$langue["id_langue"]."-".$clean_get["id"]."'>".$langue["nom_langue"]."</li>";
        }

    }
    $result .= "</ol>";
    $result .= "</div>";
    $result .= "<div id='detaillangue'>";
/*    foreach ($res_get_competence_detail as $competence){
        //$result .= $form->creer_select('langue[]', 'Langue', '', $langues, FALSE,$competence["id_langue_competence"]);*/
    $result .= $form->creer_textarea('description', 'Description','description','','','','','',30,10);
    /*}*/
    $result .= $form->fieldset_end();
    $result .= "</div>";
    $result .= $form->creer_bouton($param["action"]["close"],'bt_cancel','button','button');
    $result .= $form->creer_bouton($param["action"]["add"],'bt_addtrad','button','button');
    $result .= $form->creer_bouton($param["action"]["update"],'bt_updatetrad','button','button');
/*
} else {
        //$result .= $form->creer_select('langue[]', 'Langue', '', $langues, FALSE,'','','','','',false,false,'langue_traduction');
        $result .= $form->creer_textarea('description[]', 'Description','','','','','','',30,10,'','langue_traduction');
        $result .= $form->creer_bouton($param["action"]["cancel"],'bt_cancel','button','button');
        $result .= $form->creer_bouton($param["action"]["add"],'','submit','button');
}*/



echo $result;
?>