<h2 id="titre_page"><?=$param["title"]["listecv"]?></h2>
<input id="pagelink" type="hidden" value="candidats cv id_data cv"/> <!-- Répertoire table clef -->
<script type="text/javascript">

// $responce->rows[$i]['cell']=array($row[id_offre],$row[reference_offre],$row[date_depot_offre],
//                $row[intitule_offre],$row[lieu_offre],$row[type_offre],$row[date_debut_offre],$row[nom_contact])

    var largeurgrid = 600;
    var noms_colonnes = ['<?php echo $param["gestioncv"]["id"]?>','<?php echo $param["gestioncv"]["namefile"]?>',
        '<?php echo $param["gestioncv"]["Type"]?>','<?php echo $param["gestioncv"]["date"]?>',
        '<?php echo $param["gestioncv"]["default"]?>','<?php echo $param["gestioncv"]["default"]?>','<?php echo $param["gestioncv"]["commentaires"]?>'];

    var modele_colonnes = [{name:'id_data', index:'id_data', width:50, align:'center', search:false, hidden:true},
                           {name:'name_data', index:'name_data', align:'center', width:100},
                           {name:'type_data', index:'type_data', align:'left', width:25},
                           {name:'name_data', index:'name_data', align:'center', width:50},
                           {name:'cv_actif', index:'cv_actif', align:'center', width:50},
                           {name:'cv_actif_candidat', index:'cv_actif_candidat', hidden:true, editable:true, edittype:'checkbox', editoptions: { value:"1:null" }, editrules:{edithidden:true}},
                           {name:'notes_data', index:'notes_data', editable:true, edittype:'text',editoptions: { size:40 }}];

    var navgrid_options = {view:false,edit:true,add:false,del:true};
    var alert_ext = "<?php echo $param["erreur"]["extension"]?>";
    var alert_nbl = "<?php echo $param["erreur"]["nblimit"]?>";
    var add_title = "<?php echo $param["gestioncv"]["title"]?>";
    var data_show = "<?php echo $param["gestioncv"]["show"]?>";
    var action_upload='candidats_req-upload-cv';
    var show_subgrid=false;
    //var editligne=false; //pous savoir si on peut editer ou non en doublecliquant sur la ligne

<?php

require_once("includes/js/fonctionsjs.js");
//include("js/fonction.js");

?>
</script>
<div>
    <div id="listecv">
<!--<input type="BUTTON" id="bsdata" value="Search" /> -->
        <table id="list"></table>
        <!-- If you are using the Resizer -->
        <div id="pager"></div>
    </div>

</div>
