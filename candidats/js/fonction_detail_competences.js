/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$(function(){

    $('#bt_addtrad').hide();
    $('#bt_updatetrad').hide();

    $( "#selectable" ).selectable({
        selected: function(event, ui) {

            //Vérifie si c'est une nouvelle traduction
            var idselected = $('.ui-selected',this).attr('id');
            if(idselected.substring(0,3) != 'ns-'){
                //Récupère le texte dans la base de données
                $.post('redirecteur.php?dest=candidats_req-maj-detailcompetence', {'id':idselected,'action':'read'}, function(data){
                    $('#description').val(data);
                    $('#bt_addtrad').hide();
                    $('#bt_updatetrad').show();
                });
                return false;
            } else {
                $('#description').val('');
                $('#bt_addtrad').show();
                $('#bt_updatetrad').hide();
            }

            
        }
    });


    $('#bt_updatetrad').click(function(){

        //alert('update');
        var idselected = $('.ui-selected').attr('id');

        $.post('redirecteur.php?dest=candidats_req-maj-detailcompetence', {'id':idselected,'data':$('#description').val(),'action':'update'}, function(data){
                    alert(data);
                });
        return false;

    });

    $('#bt_addtrad').click(function(){

        alert('add');
    });


});