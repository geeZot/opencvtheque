<?php require_once 'candidats/frm-tables.php'; //contient champs formulaires, les tables, les conditions ?>
<script type="text/javascript">
    var dayNames=<?php echo $param["datepicker"]["dayNamesMin"]?>;
    var monthNames=<?php echo $param["datepicker"]["monthNames"]?>;
    var yearRange='<?php echo (date('Y')-60).':'.(date('Y')-15)?>';
    var permisRange='<?php echo (date('Y')-60).':'.date('Y')?>';
    <?php require_once("js/fonction.js"); ?>
    var largeurgrid = 0;
    var noms_colonnes = "";
    var modele_colonnes ="";
    var navgrid_options = "";
    var alert_ext = "";
    var alert_nbl = "";
    var add_title = "ajouter photo";
    var rem_title = "supprimer photo";
    var imgsrc = "candidats_edit-photo";
    var data_show = "";
    var action_upload='candidats_req-upload-photo';


    <?php require_once("includes/js/fonctionsjs.js");
          require_once("includes/js/form-fonctions.js"); ?>

    autocomplete_location('#id-city','#ville-contact','#cp-contact','','','#pays-contact');
</script>

<div id="profil">
<h2 id="titre_page"><?php echo $param["gestionprofil"]["title"]?></h2>
<input id="pagelink" type="hidden" value="candidats cv id_data cv"/> <!-- Répertoire table clef -->

<?php

    $securite = new securite();
    $cnx=new actionsdata();
    $infos_candidat=$cnx->select_data($tab_champs,$tab_tables,$tab_conditions);

    $clean_post=$securite->clean_tab($infos_candidat,"decode");

    $form=new formulaire();
    $form_candidat = $form->creer_form(
                                    array(
                                        'nomform'=>'gestionprofil',
                                        'actionform'=>'redirecteur.php?dest=candidats_req-maj-profil',
                                        'methodform'=>'POST',
                                        'class'=>'formulaire-fiche',
                                        'submit'=>$param["action"]["update"]
                                    ),
                                    $tab_champs,
                                    $clean_post[0],
                                    $param
                                    );


 
 // Affichage formulaire
    echo $form_candidat;
?>
</div>
