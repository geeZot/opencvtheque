<h2 id="titre_page">Contacts</h2>
<input id="pagelink" type="hidden" value="entreprises contacts-partenaire id_contact_partenaire bouton"/> <!-- Répertoire table clef bouton-->

<script type="text/javascript">

	<?php    if($_SESSION["type"]==2 || $_SESSION["type"]==3)   { ?>

	    var popin_largeur = 1024;
	    var popin_hauteur = 500;
	    var popin_add_title = '<?php echo $param["action"]["add"];?>';
            var popin_update_title = '<?php echo $param["action"]["update"];?>';
	    var popin_edition='entreprises_frm-partenaire&type=contact';
	    var popin_add_title_update = "Ajouter un contact";
	    var popin_rem_title_update = '<?php echo $param["action"]["delete"];?>';
	    var largeurgrid = window.innerWidth * 0.95;
            var navgrid_options = {view:false,edit:false,add:false,del:true};
            var parametre_supp = 'type=contact';
  //          var navgrid_options = {view:false,edit:false,add:false,del:false};
            var session = 23;
	<?php  } ?>



// Nom des colonnes en haut du tableau
    var noms_colonnes = ['Civilite','Nom','Prénom','Tél','Port','Fax','Email'];


// contenu des colonnes    
    var modele_colonnes = [
                           {name:'civilite', index:'entreprise', align:'left', editable:false, sortable:true, width:50},
                           {name:'nom_contact', index:'ville', align:'left', editable:false, sortable:false, width:40},
                           {name:'prenom_contact', index:'tel', align:'left', editable:false, sortable:false, search:false,width:30},
                           {name:'tel', index:'fax', align:'left', editable:false, sortable:false, search:false,width:30},
                           {name:'fax', index:'fax', align:'left', editable:false, sortable:false, search:false,width:30},
                           {name:'gsm', index:'fax', align:'left', editable:false, sortable:false, search:false,width:30},
                           {name:'email', index:'email', align:'left', editable:false, sortable:false, search:false,width:50},
                           ];

 var show_subgrid = false;
<?php
include_once (ABSPATH.'includes/js/fonctionsjs.js');
include_once ('js/fonction_liste.js');
?>


</script>

<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include_once (ABSPATH.'commun/liste.php');
?>